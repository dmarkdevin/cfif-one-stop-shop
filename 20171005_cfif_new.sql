-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3307
-- Generation Time: Oct 06, 2017 at 11:27 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cfif_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_buyers`
--

CREATE TABLE `tbl_buyers` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `username` varchar(1000) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `email` varchar(1000) DEFAULT NULL,
  `contact_number` varchar(1000) DEFAULT NULL,
  `location` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `detail` text,
  `active` int(255) NOT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_forgot_password` datetime NOT NULL,
  `token` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_image`
--

CREATE TABLE `tbl_image` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `inquiries_id` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `session` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inquiries`
--

CREATE TABLE `tbl_inquiries` (
  `id` int(255) NOT NULL,
  `dimension_width` varchar(1000) DEFAULT NULL,
  `dimension_length` varchar(1000) DEFAULT NULL,
  `dimension_height` varchar(1000) DEFAULT NULL,
  `material` varchar(1000) DEFAULT NULL,
  `lead_time` varchar(1000) DEFAULT NULL,
  `volume` varchar(1000) DEFAULT NULL,
  `target_price` varchar(1000) DEFAULT NULL,
  `additional_info` text,
  `quotation_deadline` varchar(1000) DEFAULT NULL,
  `status` varchar(1000) DEFAULT NULL,
  `company_type` varchar(1000) DEFAULT NULL,
  `active` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_materials`
--

CREATE TABLE `tbl_materials` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `token` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_members`
--

CREATE TABLE `tbl_members` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `code` varchar(1000) DEFAULT NULL,
  `username` varchar(1000) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `logo` varchar(1000) DEFAULT NULL,
  `email` varchar(1000) DEFAULT NULL,
  `contact_image` varchar(1000) DEFAULT NULL,
  `contact_person` varchar(1000) DEFAULT NULL,
  `contact_number` varchar(1000) DEFAULT NULL,
  `material_type1` varchar(1000) DEFAULT NULL,
  `material_type2` varchar(1000) DEFAULT NULL,
  `material_type3` varchar(1000) DEFAULT NULL,
  `material_type4` varchar(1000) DEFAULT NULL,
  `material_type5` varchar(1000) DEFAULT NULL,
  `capacity_rating` varchar(1000) DEFAULT NULL,
  `lead_time` varchar(1000) DEFAULT NULL,
  `company_type` varchar(1000) DEFAULT NULL,
  `shipping_type` varchar(1000) DEFAULT NULL,
  `token` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `detail` text,
  `active` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `location` varchar(1000) DEFAULT NULL,
  `image` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_members_material`
--

CREATE TABLE `tbl_members_material` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `material_id` int(255) DEFAULT NULL,
  `member_id` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `token` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quotation`
--

CREATE TABLE `tbl_quotation` (
  `id` int(255) NOT NULL,
  `member_id` int(255) DEFAULT NULL,
  `inquiries_id` int(255) DEFAULT NULL,
  `quote` varchar(1000) DEFAULT NULL,
  `status` varchar(1000) DEFAULT NULL,
  `shipping_type` varchar(1000) DEFAULT NULL,
  `comment` text,
  `payment_type` varchar(1000) DEFAULT NULL,
  `active` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(255) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `username` varchar(1000) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `email` varchar(1000) DEFAULT NULL,
  `contact_number` varchar(1000) DEFAULT NULL,
  `birth_date` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `detail` text,
  `active` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `token` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `username`, `password`, `image`, `email`, `contact_number`, `birth_date`, `type`, `detail`, `active`, `user_id`, `date_added`, `date_modified`, `token`) VALUES
(1, 'MARK DEVIN DENOSTA', NULL, '21232f297a57a5a743894a0e4a801fc3', NULL, 'dmarkdevin@gmail.com', '09464275811', NULL, 'administrator', NULL, 1, NULL, NULL, '2017-09-30 11:05:23', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_buyers`
--
ALTER TABLE `tbl_buyers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_image`
--
ALTER TABLE `tbl_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_inquiries`
--
ALTER TABLE `tbl_inquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_materials`
--
ALTER TABLE `tbl_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_members`
--
ALTER TABLE `tbl_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_members_material`
--
ALTER TABLE `tbl_members_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_quotation`
--
ALTER TABLE `tbl_quotation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_buyers`
--
ALTER TABLE `tbl_buyers`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_image`
--
ALTER TABLE `tbl_image`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_inquiries`
--
ALTER TABLE `tbl_inquiries`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_materials`
--
ALTER TABLE `tbl_materials`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_members`
--
ALTER TABLE `tbl_members`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_members_material`
--
ALTER TABLE `tbl_members_material`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_quotation`
--
ALTER TABLE `tbl_quotation`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
