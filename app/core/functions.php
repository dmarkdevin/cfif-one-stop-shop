 <?php


function profile_picture($var){
	

	if(empty($var) || !file_exists(UPLOADS.$var)){


		return 'assets/images/images.png';

		 echo $var;
	

	}else{

		return UPLOADS.$var;
	}
}


function normal_time($time){
	$time = strtotime($time);  	
		$hour = date("H",$time);
			$AmPm = $hour > 11 ? "pm" : "am";
				if(date("H:i:s",$time)=="00:00:00"){
					$result = "<div align='center'></div>";
				}else{
					$result = date("h:i:s ",$time)." ".$AmPm;
				}
				return $result; 
}	
function normal_date($date){
	if( !empty($date) AND isset($date)){
		$date = strtotime($date);  
		$date = date("M d, Y",$date);
	}else{
		$date = "";
		
	}	
	return $date;
}	


function error_message($data){
	 echo "<div class='alert alert-error alert-dismissible'>";
	 echo "<i class='icon fa fa-ban'></i>";
	 echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    foreach ( $data as $error )
	foreach ( $data as $error )
	echo "$error\n";
	echo "</div>";
}

function success_message($data){
	 echo "<div class='alert alert-success alert-dismissible' style='background-color:#DFF2BF !important;color:green !important; border: 0px !important;  '>";
	 echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    foreach ( $data as $error )
    echo "$error\n";
    echo "</div>";
}

function e($sData){
	$id=(double)$sData*525325.24;
	return base64_encode($id);
}//encrypt

function d($sData){
	$url_id=base64_decode($sData);
	$id=(double)$url_id/525325.24;
	return $id;
}//decrypt





function redirect($url)
{
	header("Location: ".URL_ROOT."$url");
}

function is_loggedin()
{
	if(isset($_SESSION[ID]))
	{
		return true;
	}else{	
			logout(); 	
	}


}

 



function if_login($data)
{
	if(isset($_SESSION[ID]))
	{
		redirect($data);
	}else{	
		return true;
	}
}



function is_administrator()
{
	if(isset($_SESSION[TYPE]) && $_SESSION[TYPE]=="ADMINISTRATOR")
	{
		return true;
	}else{	
			redirect(strtolower($_SESSION[TYPE])); 	
	}
}

function is_member()
{
	if(isset($_SESSION[TYPE]) && $_SESSION[TYPE]=="MEMBERS")
	{
		return true;
	}else{	
			redirect(strtolower($_SESSION[TYPE])); 	
	}
}
function is_buyer()
{
	if(isset($_SESSION[TYPE]) && $_SESSION[TYPE]=="BUYERS")
	{
		return true;
	}else{	
			redirect(strtolower($_SESSION[TYPE])); 	
	}
}




function name()
{
	if(isset($_SESSION[NAME]))
	{
		return ucwords(strtolower($_SESSION[NAME]) );
	}else{
		return "Unknown";
	}
}
function email_address()
{
	if(isset($_SESSION[EMAIL]))
	{
		return ($_SESSION[EMAIL]);
	}else{
		return "EMAIL ADDRESS";
	}
}
function type()
{
	if(isset($_SESSION[TYPE]))
	{
		return ucwords($_SESSION[TYPE]);
	}else{
		return "";
	}
}
function logout()
{
	session_destroy();
	// setcookie(ID,"",time() -3600);
	// setcookie(NAME,"",time() -3600);
	if(isset($_SESSION[TYPE])){
		redirect(strtolower($_SESSION[TYPE]).'/login');
	}else{
		redirect();
	}
	
	unset($_SESSION);
	
}





function lead_time($data){
	switch ($data) {
	  case '1':
	    $lead_time = 'Less Than 30 Days';
	    break;
	  case '2':
	    $lead_time = '30 - 60 Days';
	    break;
	  case '3':
	    $lead_time = '60 - 90 Days';
	    break;
	  case '4':
	    $lead_time = 'Over 90 Days';
	    break;
	  
	  
	  default:
	    $lead_time = '';
	    break;
	}


	return $lead_time;

}


function payment_type($data){
	switch ($data) {
	  case 'C':
	    $payment_type = 'Credit';
	    break;
	  case 'DP':
	    $payment_type = 'Down Payment';
	    break;
	  case 'FP':
	    $payment_type = 'Full Payment';
	    break;
	 
	  
	  
	  default:
	    $payment_type = '';
	    break;
	}


	return $payment_type;

}
function shipping_type($data){
	switch ($data) {
	  case 'fob_cebu':
	    $shipping_type = 'FOB Cebu';
	    break;
	  case 'door_to_door':
	    $shipping_type = 'Door to door';
	    break;
	 
	 
	  
	  
	  default:
	    $shipping_type = '';
	    break;
	}


	return $shipping_type;

}


function account_type($data){
	switch ($data) {
	  case 'members':
	    $var = 'MEMBER';
	    break;
	  case 'buyers':
	    $var = 'BUYER';
	    break;
	  case 'administrator':
	    $var = 'ADMINISTRATOR';
	    break;
	  
	  
	  
	  default:
	    $var = '';
	    break;
	}


	return $var;

}


function submitted(){
	
		echo '<span class="badge bg-blue">Submitted</span>';
	 
}
function selected(){
	
		echo '<span class="badge bg-yellow-active">Selected</span>';
	 
}



function inquiry_status($data){
	




	if(isset($data)){ 
		// echo '<span class="label label-success border-radius">Closed</span>';
		return '<span class="badge bg-green">Closed</span>';
	}else{ 
		// echo '<span class="label label-warning border-radius">Pending</span>';
			return '<span class="badge bg-orange">Pending</span>';
	}




}



function age($date){
			$from = new DateTime($date);
			$to   = new DateTime('today');
			return $from->diff($to)->d."\n";
}


function table($data){
	switch (strtolower($data)) {
	  case 'members':
	    $var = 'tbl_members';
	    break;
	  case 'buyers':
	    $var = 'tbl_buyers';
	    break;
	  case 'administrator':
	    $var = 'tbl_users';
	    break;
	  
	  
	  
	  default:
	    $var = '';
	    break;
	}


	return $var;

}

function inc($var){
	

	if(file_exists(URL_ROOT.'views/'.$var . '.php'))
		{
			 return include($var);

		}else {
			 return '';
		}
}




function readmore($string, $word_limit = 5)
    {
        $words = explode(" ",$string);
        
        return implode(" ",array_splice($words,0,$word_limit)).' ..';
    }




















 
/**
 * easy image resize function
 * @param  $file - file name to resize
 * @param  $string - The image data, as a string
 * @param  $width - new image width
 * @param  $height - new image height
 * @param  $proportional - keep image proportional, default is no
 * @param  $output - name of the new file (include path if needed)
 * @param  $delete_original - if true the original image will be deleted
 * @param  $use_linux_commands - if set to true will use "rm" to delete the image, if false will use PHP unlink
 * @param  $quality - enter 1-100 (100 is best quality) default is 100
 * @return boolean|resource
 */
  function smart_resize_image($file,
                              $string             = null,
                              $width              = 0, 
                              $height             = 0, 
                              $proportional       = false, 
                              $output             = 'file', 
                              $delete_original    = true, 
                              $use_linux_commands = false,
  							  $quality = 100
  		 ) {
      
    if ( $height <= 0 && $width <= 0 ) return false;
    if ( $file === null && $string === null ) return false;

    # Setting defaults and meta
    $info                         = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
    $image                        = '';
    $final_width                  = 0;
    $final_height                 = 0;
    list($width_old, $height_old) = $info;
	$cropHeight = $cropWidth = 0;

    # Calculating proportionality
    if ($proportional) {
      if      ($width  == 0)  $factor = $height/$height_old;
      elseif  ($height == 0)  $factor = $width/$width_old;
      else                    $factor = min( $width / $width_old, $height / $height_old );

      $final_width  = round( $width_old * $factor );
      $final_height = round( $height_old * $factor );
    }
    else {
      $final_width = ( $width <= 0 ) ? $width_old : $width;
      $final_height = ( $height <= 0 ) ? $height_old : $height;
	  $widthX = $width_old / $width;
	  $heightX = $height_old / $height;
	  
	  $x = min($widthX, $heightX);
	  $cropWidth = ($width_old - $width * $x) / 2;
	  $cropHeight = ($height_old - $height * $x) / 2;
    }

    # Loading image to memory according to type
    switch ( $info[2] ) {
      case IMAGETYPE_JPEG:  $file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);  break;
      case IMAGETYPE_GIF:   $file !== null ? $image = imagecreatefromgif($file)  : $image = imagecreatefromstring($string);  break;
      case IMAGETYPE_PNG:   $file !== null ? $image = imagecreatefrompng($file)  : $image = imagecreatefromstring($string);  break;
      default: return false;
    }
    
    
    # This is the resizing/resampling/transparency-preserving magic
    $image_resized = imagecreatetruecolor( $final_width, $final_height );
    if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
      $transparency = imagecolortransparent($image);
      $palletsize = imagecolorstotal($image);

      if ($transparency >= 0 && $transparency < $palletsize) {
        $transparent_color  = imagecolorsforindex($image, $transparency);
        $transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
        imagefill($image_resized, 0, 0, $transparency);
        imagecolortransparent($image_resized, $transparency);
      }
      elseif ($info[2] == IMAGETYPE_PNG) {
        imagealphablending($image_resized, false);
        $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
        imagefill($image_resized, 0, 0, $color);
        imagesavealpha($image_resized, true);
      }
    }
    imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);
	
	
    # Taking care of original, if needed
    if ( $delete_original ) {
      if ( $use_linux_commands ) exec('rm '.$file);
      else @unlink($file);
    }

    # Preparing a method of providing result
    switch ( strtolower($output) ) {
      case 'browser':
        $mime = image_type_to_mime_type($info[2]);
        header("Content-type: $mime");
        $output = NULL;
      break;
      case 'file':
        $output = $file;
      break;
      case 'return':
        return $image_resized;
      break;
      default:
      break;
    }
    
    # Writing image according to type to the output destination and image quality
    switch ( $info[2] ) {
      case IMAGETYPE_GIF:   imagegif($image_resized, $output);    break;
      case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output, $quality);   break;
      case IMAGETYPE_PNG:
        $quality = 9 - (int)((0.9*$quality)/10.0);
        imagepng($image_resized, $output, $quality);
        break;
      default: return false;
    }

    return true;
  }

  function country(){


		$countryList = array(
		"AF" => "Afghanistan",
		"AL" => "Albania",
		"DZ" => "Algeria",
		"AS" => "American Samoa",
		"AD" => "Andorra",
		"AO" => "Angola",
		"AI" => "Anguilla",
		"AQ" => "Antarctica",
		"AG" => "Antigua and Barbuda",
		"AR" => "Argentina",
		"AM" => "Armenia",
		"AW" => "Aruba",
		"AU" => "Australia",
		"AT" => "Austria",
		"AZ" => "Azerbaijan",
		"BS" => "Bahamas",
		"BH" => "Bahrain",
		"BD" => "Bangladesh",
		"BB" => "Barbados",
		"BY" => "Belarus",
		"BE" => "Belgium",
		"BZ" => "Belize",
		"BJ" => "Benin",
		"BM" => "Bermuda",
		"BT" => "Bhutan",
		"BO" => "Bolivia",
		"BA" => "Bosnia and Herzegovina",
		"BW" => "Botswana",
		"BV" => "Bouvet Island",
		"BR" => "Brazil",
		"BQ" => "British Antarctic Territory",
		"IO" => "British Indian Ocean Territory",
		"VG" => "British Virgin Islands",
		"BN" => "Brunei",
		"BG" => "Bulgaria",
		"BF" => "Burkina Faso",
		"BI" => "Burundi",
		"KH" => "Cambodia",
		"CM" => "Cameroon",
		"CA" => "Canada",
		"CT" => "Canton and Enderbury Islands",
		"CV" => "Cape Verde",
		"KY" => "Cayman Islands",
		"CF" => "Central African Republic",
		"TD" => "Chad",
		"CL" => "Chile",
		"CN" => "China",
		"CX" => "Christmas Island",
		"CC" => "Cocos [Keeling] Islands",
		"CO" => "Colombia",
		"KM" => "Comoros",
		"CG" => "Congo - Brazzaville",
		"CD" => "Congo - Kinshasa",
		"CK" => "Cook Islands",
		"CR" => "Costa Rica",
		"HR" => "Croatia",
		"CU" => "Cuba",
		"CY" => "Cyprus",
		"CZ" => "Czech Republic",
		"CI" => "Côte d’Ivoire",
		"DK" => "Denmark",
		"DJ" => "Djibouti",
		"DM" => "Dominica",
		"DO" => "Dominican Republic",
		"NQ" => "Dronning Maud Land",
		"DD" => "East Germany",
		"EC" => "Ecuador",
		"EG" => "Egypt",
		"SV" => "El Salvador",
		"GQ" => "Equatorial Guinea",
		"ER" => "Eritrea",
		"EE" => "Estonia",
		"ET" => "Ethiopia",
		"FK" => "Falkland Islands",
		"FO" => "Faroe Islands",
		"FJ" => "Fiji",
		"FI" => "Finland",
		"FR" => "France",
		"GF" => "French Guiana",
		"PF" => "French Polynesia",
		"TF" => "French Southern Territories",
		"FQ" => "French Southern and Antarctic Territories",
		"GA" => "Gabon",
		"GM" => "Gambia",
		"GE" => "Georgia",
		"DE" => "Germany",
		"GH" => "Ghana",
		"GI" => "Gibraltar",
		"GR" => "Greece",
		"GL" => "Greenland",
		"GD" => "Grenada",
		"GP" => "Guadeloupe",
		"GU" => "Guam",
		"GT" => "Guatemala",
		"GG" => "Guernsey",
		"GN" => "Guinea",
		"GW" => "Guinea-Bissau",
		"GY" => "Guyana",
		"HT" => "Haiti",
		"HM" => "Heard Island and McDonald Islands",
		"HN" => "Honduras",
		"HK" => "Hong Kong SAR China",
		"HU" => "Hungary",
		"IS" => "Iceland",
		"IN" => "India",
		"ID" => "Indonesia",
		"IR" => "Iran",
		"IQ" => "Iraq",
		"IE" => "Ireland",
		"IM" => "Isle of Man",
		"IL" => "Israel",
		"IT" => "Italy",
		"JM" => "Jamaica",
		"JP" => "Japan",
		"JE" => "Jersey",
		"JT" => "Johnston Island",
		"JO" => "Jordan",
		"KZ" => "Kazakhstan",
		"KE" => "Kenya",
		"KI" => "Kiribati",
		"KW" => "Kuwait",
		"KG" => "Kyrgyzstan",
		"LA" => "Laos",
		"LV" => "Latvia",
		"LB" => "Lebanon",
		"LS" => "Lesotho",
		"LR" => "Liberia",
		"LY" => "Libya",
		"LI" => "Liechtenstein",
		"LT" => "Lithuania",
		"LU" => "Luxembourg",
		"MO" => "Macau SAR China",
		"MK" => "Macedonia",
		"MG" => "Madagascar",
		"MW" => "Malawi",
		"MY" => "Malaysia",
		"MV" => "Maldives",
		"ML" => "Mali",
		"MT" => "Malta",
		"MH" => "Marshall Islands",
		"MQ" => "Martinique",
		"MR" => "Mauritania",
		"MU" => "Mauritius",
		"YT" => "Mayotte",
		"FX" => "Metropolitan France",
		"MX" => "Mexico",
		"FM" => "Micronesia",
		"MI" => "Midway Islands",
		"MD" => "Moldova",
		"MC" => "Monaco",
		"MN" => "Mongolia",
		"ME" => "Montenegro",
		"MS" => "Montserrat",
		"MA" => "Morocco",
		"MZ" => "Mozambique",
		"MM" => "Myanmar [Burma]",
		"NA" => "Namibia",
		"NR" => "Nauru",
		"NP" => "Nepal",
		"NL" => "Netherlands",
		"AN" => "Netherlands Antilles",
		"NT" => "Neutral Zone",
		"NC" => "New Caledonia",
		"NZ" => "New Zealand",
		"NI" => "Nicaragua",
		"NE" => "Niger",
		"NG" => "Nigeria",
		"NU" => "Niue",
		"NF" => "Norfolk Island",
		"KP" => "North Korea",
		"VD" => "North Vietnam",
		"MP" => "Northern Mariana Islands",
		"NO" => "Norway",
		"OM" => "Oman",
		"PC" => "Pacific Islands Trust Territory",
		"PK" => "Pakistan",
		"PW" => "Palau",
		"PS" => "Palestinian Territories",
		"PA" => "Panama",
		"PZ" => "Panama Canal Zone",
		"PG" => "Papua New Guinea",
		"PY" => "Paraguay",
		"YD" => "People's Democratic Republic of Yemen",
		"PE" => "Peru",
		"PH" => "Philippines",
		"PN" => "Pitcairn Islands",
		"PL" => "Poland",
		"PT" => "Portugal",
		"PR" => "Puerto Rico",
		"QA" => "Qatar",
		"RO" => "Romania",
		"RU" => "Russia",
		"RW" => "Rwanda",
		"RE" => "Réunion",
		"BL" => "Saint Barthélemy",
		"SH" => "Saint Helena",
		"KN" => "Saint Kitts and Nevis",
		"LC" => "Saint Lucia",
		"MF" => "Saint Martin",
		"PM" => "Saint Pierre and Miquelon",
		"VC" => "Saint Vincent and the Grenadines",
		"WS" => "Samoa",
		"SM" => "San Marino",
		"SA" => "Saudi Arabia",
		"SN" => "Senegal",
		"RS" => "Serbia",
		"CS" => "Serbia and Montenegro",
		"SC" => "Seychelles",
		"SL" => "Sierra Leone",
		"SG" => "Singapore",
		"SK" => "Slovakia",
		"SI" => "Slovenia",
		"SB" => "Solomon Islands",
		"SO" => "Somalia",
		"ZA" => "South Africa",
		"GS" => "South Georgia and the South Sandwich Islands",
		"KR" => "South Korea",
		"ES" => "Spain",
		"LK" => "Sri Lanka",
		"SD" => "Sudan",
		"SR" => "Suriname",
		"SJ" => "Svalbard and Jan Mayen",
		"SZ" => "Swaziland",
		"SE" => "Sweden",
		"CH" => "Switzerland",
		"SY" => "Syria",
		"ST" => "São Tomé and Príncipe",
		"TW" => "Taiwan",
		"TJ" => "Tajikistan",
		"TZ" => "Tanzania",
		"TH" => "Thailand",
		"TL" => "Timor-Leste",
		"TG" => "Togo",
		"TK" => "Tokelau",
		"TO" => "Tonga",
		"TT" => "Trinidad and Tobago",
		"TN" => "Tunisia",
		"TR" => "Turkey",
		"TM" => "Turkmenistan",
		"TC" => "Turks and Caicos Islands",
		"TV" => "Tuvalu",
		"UM" => "U.S. Minor Outlying Islands",
		"PU" => "U.S. Miscellaneous Pacific Islands",
		"VI" => "U.S. Virgin Islands",
		"UG" => "Uganda",
		"UA" => "Ukraine",
		"SU" => "Union of Soviet Socialist Republics",
		"AE" => "United Arab Emirates",
		"GB" => "United Kingdom",
		"US" => "United States",
		"ZZ" => "Unknown or Invalid Region",
		"UY" => "Uruguay",
		"UZ" => "Uzbekistan",
		"VU" => "Vanuatu",
		"VA" => "Vatican City",
		"VE" => "Venezuela",
		"VN" => "Vietnam",
		"WK" => "Wake Island",
		"WF" => "Wallis and Futuna",
		"EH" => "Western Sahara",
		"YE" => "Yemen",
		"ZM" => "Zambia",
		"ZW" => "Zimbabwe",
		"AX" => "Åland Islands",
		);
 	return $countryList;

  }
function add_button($location,$message){
$a ='<a class="btn btn-info btn-flat btn-sm pull-right" title="" tooltip href="'.URL_ROOT.$location.'" style="background-color:#128CDF;">
        <i class="fa fa-plus"></i> '.$message.'</a> ';
return $a;
}

function update_button($location,$id){

$a = '<a class="btn btn-info btn-flat btn-xs" title="" tooltip href="'.URL_ROOT.$location.'/'.e($id).'" style="background-color:#128CDF;">
<i class="fa fa-pencil"></i> Modify</a>';


return $a;

}

function delete_button($location,$id){

$a = '


<button type="button" class="btn btn-danger btn-flat btn-xs" data-toggle="modal" data-target="#deleteModal'.$id.'"><i class="fa fa-trash"></i>Remove</button>            

<div id="deleteModal'.$id.'" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete !</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to Delete ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">NO</button>
        <a class="btn btn-danger" title="" tooltip href="'.URL_ROOT.$location.'/'.e($id).'">
		DELETE</a>
      </div>
    </div>

  </div>
</div>

';


return $a;

}



  function materials(){

		$materialsList = array(
			"wood" 		=> "Wood",
			"rattan" 	=> "Rattan",
			"metal" 	=> "Metal"
		);

 	return $materialsList;

  }




  function transaction_status($data){
	
	if(isset($data)){ 
		// echo '<span class="label label-success border-radius">Closed</span>';
		echo '<span class="badge bg-green">Closed</span>';
	}else{ 
		// echo '<span class="label label-warning border-radius">Pending</span>';
			echo '<span class="badge bg-orange">Pending</span>';
	}

}