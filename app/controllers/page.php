<?php
class Page extends Controller
{ 
	public function __construct(){
		$this->db 	= $this->model('db');
		$this->url 	= $this->url();		

	}

	public function index(){
	 
		 
		$this->view('page/landing_page');
		 

	}
	public function test(){
	 
		 
		$this->view('page/test');
		 

	}
	public function test2(){
	 
		 
		$this->view('page/test2');
		 

	}
	public function index2(){
	 
		$this->view('include/header');
		$this->view('page/index');
		$this->view('page/index2');
		$this->view('include/footer');

	}
	public function top_nav(){
	 
	 
		$this->view('page/top_nav');
		 

	}
	public function modal(){
	 
		$this->view('include/header');
		$this->view('page/modal');
		$this->view('include/footer');

	}
	public function widget(){
	 
		$this->view('include/header');
		$this->view('page/widget');
		$this->view('include/footer');

	}
	public function icons(){
	 
		$this->view('include/header');
		$this->view('page/icons');
		$this->view('include/footer');

	}
	public function ui_general(){
	 
		$this->view('include/header');
		$this->view('page/general');
		$this->view('include/footer');

	}
	public function buttons(){
	 
		$this->view('include/header');
		$this->view('page/buttons');
		$this->view('include/footer');

	}

	public function general(){
	 
		$this->view('include/header');
		$this->view('page/forms/general');
		$this->view('include/footer');

	}
	public function datatable(){
	 
		$this->view('include/header');
		$this->view('page/tables/datatable');
		$this->view('include/footer');

	}
	public function simpletable(){
	 
		$this->view('include/header');
		$this->view('page/tables/datatable');
		$this->view('page/tables/simpletable');
		$this->view('include/footer');

	}
	public function invoice(){
	 
		$this->view('include/header');
		$this->view('page/invoice');
		$this->view('include/footer');

	}
	public function mailbox(){
	 
		$this->view('include/header');
		$this->view('page/mailbox');
		$this->view('include/footer');

	}	
	public function read_mail(){
	 
		$this->view('include/header');
		$this->view('page/read_mail');
		$this->view('include/footer');

	}	
	public function compose(){
	 
		$this->view('include/header');
		$this->view('page/compose');
		$this->view('include/footer');

	}	
	public function profile(){
	 
		$this->view('include/header');
		$this->view('page/profile');
		$this->view('include/footer');

	}
	public function login(){
	 
		$this->view('include/header');
		$this->view('page/login');
		$this->view('include/footer');

	}	
	public function register(){
	 
		$this->view('include/header');
		$this->view('page/register');
		$this->view('include/footer');

	}	
	public function about(){
	 
		$this->view('main/header');
		$this->view('page/about');
		$this->view('main/footer');

	}	

	
	public function buyers(){
		
		$data['title'] = 'database';
		$data['buyers'] = $this->db->getRows("tbl_buyers");
		$this->view('page/buyers_lists',$data);
		 
	} 

	public function members(){
		
		$data['title'] = 'database';
		$data['members'] = $this->db->getRows("tbl_members"); 
		$this->view('page/members_lists',$data);
		 
	} 

	public function inquiries(){
		
		$data['title'] = 'database';
		$data['inquiries'] = $this->db->getRows("tbl_inquiries"); 
		
		echo '<pre>';
		print_r($data['inquiries']);
		echo '</pre>'; 
	} 
	public function quotation(){
		
		$data['title'] = 'database';
		$data['quotation'] = $this->db->getRows("tbl_quotation"); 
		
		echo '<pre>';
		print_r($data['quotation']);
		echo '</pre>'; 
	} 
	public function deleteall(){
		$this->db->delete('tbl_buyers');
		$this->db->delete('tbl_members');
		$this->db->delete('tbl_quotation');
		$this->db->delete('tbl_inquiries');
		$this->db->delete('tbl_image');
		$this->db->delete('tbl_materials');

		$this->db->delete('tbl_members_material');
		unlink(URL_ROOT.UPLOADS.'/*');

		
		// unlink('uploads/*');
		redirect('page');
	}
	public function database()
	{
		

		$data['title'] = 'database';

		$database = $this->db->getQuery("SHOW TABLES FROM ".DB_NAME);

		 


		$data['buyers'] = $this->db->getRows("tbl_buyers");
		$data['members'] = $this->db->getRows("tbl_members");
		$data['transactions'] = $this->db->getQuery("SELECT * FROM tbl_inquiries ORDER BY id DESC  LIMIT 1 ");
		$data['quotation'] = $this->db->getQuery("SELECT * FROM tbl_quotation ORDER BY id  DESC LIMIT 10 ");
	
		$this->view('buyers/main/header');
		$this->view('page/database',$data);
		$this->view('buyers/main/footer');
		 
	} 




	public function delete()
	{
		

 
		$this->view('page/delete');
	 
		 
	} 
 











 }