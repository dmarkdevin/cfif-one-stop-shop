<?php
class Buyers extends Controller
{ 
	public function __construct(){
		$this->db 	= $this->model('db');
		$this->url 	= $this->url();		

	}

	public function index(){
 
		$data['title'] = 'Dashboard';
 		is_buyer();
 		is_loggedin();
		$query 			= "SELECT * FROM tbl_inquiries WHERE user_id='".$_SESSION[ID]."' ORDER BY date_added DESC LIMIT 10 ";
 		$data['list']	= $this->db->getQuery($query);
		$data['count']	= $this->db->getCount($query);
		$data['country'] = country();
		$data['materials'] = materials();
		$materials 			= "SELECT * FROM tbl_materials   ";
 		$data['materials']	= $this->db->getQuery($materials);
		$this->view('buyers/header',$data);
		$this->view('buyers/index',$data);
		$this->view('buyers/footer');
	}

	public function inquire(){

		is_buyer();
 		is_loggedin();
		$data['title'] = 'Inquire';
		$data['message'] = true;
		$buyer 		 = "SELECT * FROM tbl_buyers WHERE id='".$_SESSION[ID]."'";
 		$data['buyer']	 = $this->db->getFetch($buyer);
		$data['buyers']  = $this->db->getCount($buyer);


		if(isset($_POST['submit'])){
			$Data = array(
				'dimension_height'	=> $_POST['dimension_height'],
				'dimension_width'	=> $_POST['dimension_width'],
				'dimension_length'	=> $_POST['dimension_length'],
				'volume'			=> $_POST['quantity'],
				'material'			=> $_POST['material'],
				'target_price'		=> $_POST['target_price'],
				'lead_time'			=> $_POST['lead_time'],
				'company_type'		=> $_POST['company_type'],
				'additional_info'	=> $_POST['additional_info'],
			 	'user_id'			=>$_SESSION[ID] );
			$_SESSION['inquire'] 	= $Data;
			$_SESSION['location']	 =$data['buyer']['location'];
			if(!isset($_SESSION['file'])){
			 	$_SESSION['file'] 	=  ($_SESSION[ID].time());
			}
			if(isset($_FILES['image']) ){
				foreach($_FILES['image']['tmp_name'] as $key => $tmp_name ){
					$imgFile = basename($_FILES['image']['name'][$key]);
					$tmp_dir = $_FILES['image']['tmp_name'][$key];
					$imgSize = $_FILES['image']['size'][$key];
					$upload_dir = UPLOADS; // upload directory
					$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
					// valid image extensions
					$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
					// rename uploading image
					$userpic = time().rand(1000,1000000).".".$imgExt;
					// allow valid image file formats
					if(in_array($imgExt, $valid_extensions)){			
						// Check file size '5MB'
						if($imgSize < 5000000){		
							$Data = array(
								'name' 		=> $userpic ,
								'session' 	=> $_SESSION['file'],
								'user_id'	=> $_SESSION[ID]
							);
							$add = $this->db->insert('tbl_image' ,$Data);
								if(move_uploaded_file($tmp_dir,$upload_dir.$userpic)){
								} 
						}	
					}		
				}
			}	
			redirect('buyers/inquire2');
		}

 
		$materials 			= "SELECT * FROM tbl_materials   ";
 		$data['materials']	= $this->db->getQuery($materials);
		$this->view('buyers/header',$data);
		$this->view('buyers/inquire',$data);
		$this->view('buyers/footer');
	}

	public function inquire2(){
	
		is_buyer();
 		is_loggedin();
		if(!isset($_SESSION['inquire'])): redirect('buyers/inquire'); endif;

		$data['inquiry'] = ($_SESSION['inquire']);
		$data['location'] = $_SESSION['location'];

		$quantity 			= $data['inquiry']['volume'];
		$target_price 		= $data['inquiry']['target_price'];
		$unit_value_points 	= 140;
		$lead_time	 		= $data['inquiry']['lead_time'];
		$unit_value 		= $quantity * $target_price;
		$capacity_rating 	= $unit_value / $unit_value_points; 
		$capacity_rating 	= $capacity_rating / $lead_time;
		$location 			= $data['location'];

 		

		if($data['location'] == 'PH'){
			$shipping_type = " shipping_type = 'all' OR shipping_type = 'domestic' ";
		}else{
			$shipping_type = " shipping_type = 'export' ";
		}


		switch ($data['inquiry']['lead_time']) {
			case '1':
				$lead_time = "lead_time = 1 OR lead_time =2";
				break;
			case '2':
				$lead_time = "lead_time = 2 OR lead_time =3";
				break;
			case '3':
				$lead_time = "lead_time = 3 OR lead_time =4";
				break;
			case '4':
				$lead_time = "lead_time = 4 OR lead_time =3";
				break;												
			default:
				# code...
				break;
		}

		switch ($data['inquiry']['company_type']) {
			case 'any':
				$company_type = "company_type = 'any' OR company_type = 'residential' OR company_type = 'commercial'";
				break;
			case 'residential':
				$company_type = "company_type = 'residential' ";
				break;
			case 'commercial':
				$company_type = "company_type = 'commercial' ";
				break;	
			default:
				# code...
				break;
		}
 
		$member = "SELECT * FROM tbl_members WHERE (".$shipping_type.") AND (".$lead_time.") AND (".$company_type.")  AND (company_type = '".$data['inquiry']['company_type']."' OR company_type = 'any' ) " ;

		$data['member']	= $this->db->getQuery($member);	
		$data['members']	= $this->db->getCount($member);	


		$material = "SELECT * FROM tbl_materials WHERE id ='".$data['inquiry']['material']."' " ;
		$data['material']	= $this->db->getFetch($material);

		$i=0;$my_var='';
		foreach ($data['member'] as $key => $value) {
			# code...
			
		 
			$members_material = "SELECT * FROM tbl_members_material WHERE material_id ='".$data['material']['id']."' AND member_id ='".$value['id']."'" ;
			 $data['members_materials']	= $this->db->getCount($members_material);			
			 $data['members_material']	= $this->db->getQuery($members_material);			

			 if( $data['members_materials']>0){
					// echo $value['id'].'<br>';
					// echo $data['members_material']['member_id'];

			 	// echo($data['members_material'][0]['member_id']).'<br>';
			 	 $pre = ($i > 0)?',':'';
			 	 $my_var .=  $pre.$value['id'];


					$i += 1;
					// $i++;
			 }



		}

$data['count'] = $i;

// echo '('.$i.')';
 


		// ---------------------------------------------------------------------------------------		
	 // 	$material = "SELECT * FROM tbl_materials WHERE id ='".$data['inquiry']['material']."' " ;
		// $data['material']	= $this->db->getFetch($material);	
		// $members_material = "SELECT * FROM tbl_members_material WHERE material_id ='".$data['material']['id']."' AND member_id ='".$data['member']['id']."'" ;
		// $data['members_material']	= $this->db->getFetch($members_material);	
		// $data['members_materials']	= $this->db->getCount($members_material);			 
		// ---------------------------------------------------------------------------------------
 
			if($data['members']>0 )  {
					$data['member'] = $this->db->getQuery($member) ;
					// $_SESSION['inquire2'] =  $data['member'];
					$_SESSION['inquire2'] = $my_var;

					// print_r($_SESSION['inquire2']);
			}		
		$materials 			= "SELECT * FROM tbl_materials   ";
 		$data['materials']	= $this->db->getQuery($materials);
		$data['images'] = $this->db->getQuery("SELECT * FROM tbl_image WHERE user_id = '".$_SESSION[ID]."' AND session = '".$_SESSION['file']."'  ORDER BY id DESC LIMIT 3 ") ;
		$this->view('buyers/header',$data);
		$this->view('buyers/inquire2',$data);
		$this->view('buyers/footer');
	}

	public function inquire3(){
		is_buyer();
 		is_loggedin();
 		// print_r($_SESSION['inquire2']);
		if(isset($_SESSION['inquire2'])){
			$data['members'] = $_SESSION['inquire2']; 
			$data['inquiry'] = $_SESSION['inquire']; 
			$data['images'] = $this->db->getQuery("SELECT * FROM tbl_image WHERE user_id = '".$_SESSION[ID]."' AND session = '".$_SESSION['file']."'  ORDER BY id DESC LIMIT 3 ") ;
		}else{
			redirect('buyers/inquire');
		}


		$this->view('buyers/header',$data);
		$this->view('buyers/inquire3',$data);
		$this->view('buyers/footer');
	}

	public function inquire4(){


	
		is_buyer();
 		is_loggedin();
		$data['message'] = true;
		$query 			= "SELECT * FROM tbl_inquiries WHERE user_id='".$_SESSION[ID]."' ORDER BY id DESC  LIMIT 10 ";
 		$data['list']	= $this->db->getQuery($query);
		$data['count']	= $this->db->getCount($query);
 		if(!isset($_SESSION['inquire2']) && !isset($_SESSION['inquire'])){
 			redirect('buyers/index');
 		}	
		$add = $this->db->insert('tbl_inquiries' ,$_SESSION['inquire']);
		$lastID = $this->db->conn->lastInsertId();
		$data['images'] = $this->db->getQuery("SELECT * FROM tbl_image WHERE user_id = '".$_SESSION[ID]."' AND session = '".$_SESSION['file']."' LIMIT 3 ") ;
		if($lastID == 0){
			$lastID = 1;
		}
		foreach ($data['images'] as $image) {
			$Data = array(
				'inquiries_id'=> $lastID,
				'session'=> ''
			);
			$edit = $this->db->update('tbl_image',$Data,array('id' => $image['id'] ));
		}

		if($add){
			$data['inquiries'] = $_SESSION['inquire2'];

			$data['inquiries'] = explode(",", $_SESSION['inquire2']);

				foreach ($data['inquiries'] as $key ) {
				$Data2 = array(
					'member_id' 		=> $key,
					'inquiries_id' 	=> $lastID
				);
					$add2 = $this->db->insert('tbl_quotation' ,$Data2);
					$member 		= "SELECT * FROM tbl_members WHERE id='".$key."' ";
			 		$data['member']	= $this->db->getFetch($member);
					$data['count']	= $this->db->getCount($member);

					if($add2){
						email_to_qualified_member($lastID,$data['member']);
					}
				}
 
					unset($_SESSION['inquire']);
					unset($_SESSION['inquire2']);	

					// print_r($_SESSION['inquire2']);	
					// unset($_SESSION['file']);
					unset($_SESSION['tmp_name']);

					$query 			= "SELECT * FROM tbl_inquiries WHERE user_id='".$_SESSION[ID]."' ORDER BY id DESC ";
 					$data['list']	= $this->db->getQuery($query);
					$data['count']	= $this->db->getCount($query);

					$materials 			= "SELECT * FROM tbl_materials   ";
 					$data['materials']	= $this->db->getQuery($materials);
		}

		$this->view('buyers/header',$data);
		$this->view('buyers/index',$data);
		$this->view('buyers/footer');

	}
	public function inquiry(){
	
		is_buyer();
 		is_loggedin();
		isset($this->url[2]) ? '':redirect('buyers/index');
		$id = d($this->url[2]);
		$inquiries 			= "SELECT * FROM tbl_inquiries WHERE id='".$id."' ";
 		$data['inquiries']	= $this->db->getCount($inquiries);
		$data['inquiry']	= $this->db->getFetch($inquiries);
		$data['inquiries'] 	== true? '':redirect('buyers/index');
		$quotation 			= "SELECT * FROM tbl_quotation WHERE inquiries_id = '".$data['inquiry']['id']."'  AND quote IS NOT NULL";
 		$data['quotation']	= $this->db->getQuery($quotation);
 		$data['quotations']	= $this->db->getCount($quotation);
 		$data['images']		= $this->db->getQuery("SELECT * FROM tbl_image  WHERE inquiries_id ='".$data['inquiry']['id']."' ") ;
 		 $days = days($data['inquiry']['date_added']);

                if($days > 3 && $data['inquiry']['status'] == ''){
                   redirect('buyers/expire/'.e($data['inquiry']['id']));
                } 
		$this->view('buyers/header',$data);
		$this->view('buyers/inquiry',$data);
		$this->view('buyers/footer');
	}

	public function select_quotation(){

		is_buyer();
 		is_loggedin();
		isset($this->url[2]) ? '':redirect('buyers/index');
		$id = d($this->url[2]);
		$quotations 		= "SELECT * FROM tbl_quotation WHERE id='".$id."' ";
 		$data['quotations']	= $this->db->getCount($quotations);
		$data['quotation']	= $this->db->getFetch($quotations);
		$data['quotations'] == true? '':redirect('buyers/index');
		$inquiries 	 		= "SELECT * FROM tbl_inquiries WHERE id='".$data['quotation']['inquiries_id']."' ";
 		$data['inquiries']	= $this->db->getCount($inquiries);
		$data['inquiry']	= $this->db->getFetch($inquiries);
		$members 			= "SELECT * FROM tbl_members WHERE id='".$data['quotation']['member_id']."' ";
 		$data['members']	= $this->db->getCount($members);
		$data['member']		= $this->db->getFetch($members);
		$edit 	= $this->db->update('tbl_quotation',array('status' => 'selected'),array('id' => $id));
		$edit2 	= $this->db->update('tbl_inquiries',array('status' => 'closed'),array('id' => $data['quotation']['inquiries_id']));

		if($edit && $edit2){
			email_to_selected_member($data['quotation'],$data['member']);
			redirect('buyers/inquiry/'.e($data['quotation']['inquiries_id'])); 
		}
	}

	public function inquiries(){

		$data['title'] = 'Inquiries';
		is_buyer();
 		is_loggedin();
		$query 			= "SELECT * FROM tbl_inquiries WHERE user_id='".$_SESSION[ID]."' ORDER BY date_added DESC";
 		$data['list']	= $this->db->getQuery($query);
		$data['count']	= $this->db->getCount($query);
		$quotation 			= "SELECT * FROM tbl_quotation WHERE user_id='".$_SESSION[ID]."' ";
 		$data['quotation']	= $this->db->getQuery($quotation);
		$this->view('buyers/header',$data);
		$this->view('buyers/inquiries',$data);
		$this->view('buyers/footer');
	} 

	public function login(){
	
		if_login($this->url[0]);
		$data['title'] = 'LOGIN';
		if(isset($_POST['submit'])){
			$table = table($this->url[0]);
			$query = "SELECT email,password,id,name,type,email,active FROM tbl_buyers WHERE  email ='". $_POST['email']."' AND password ='".md5($_POST['password'])."' ";
 			$data['user']	= $this->db->getFetch($query);
			$data['count']	= $this->db->getCount($query);
			if($data['count'] > 0){
	 			if($data['user']['active']==1){
					$_SESSION[ID]    =  ($data['user']['id']);
					$_SESSION[NAME]  =  ucwords($data['user']['name']);
					$_SESSION[TYPE]  =  strtoupper($data['user']['type']);
					$_SESSION[EMAIL] =  strtolower($data['user']['email']);
					redirect('buyers/index');
				}else{
			 		$error[] =  "Your account is not yet active";
					$data['error'] = $error;
		 		}
			}else{
				$error[] =  "Invalid email or password.";
				$data['error'] = $error;
			}
		}
		$this->view('buyers/main/header');
		$this->view('buyers/login',$data);
		$this->view('buyers/main/footer');
	}

 	public function logout(){
		logout();
	}

	public function register(){
	
		$data['title'] = 'Sign Up';
			if (isset($_POST['submit']) )  {
				$Data = array(
					'name' 			=> $_POST['name'],
					'email' 		=> $_POST['email'],
					'contact_number'=> $_POST['contact_number'],
					'location' 		=> $_POST['location'],
					'password' 		=> md5($_POST['password']),
					'type' 			=> "buyers",
					'token'			=> md5($_POST['email'].uniqid(rand()))
				);
			$query 			= "SELECT * FROM tbl_buyers WHERE email='".$Data['email']."'";
			$data['count']	= $this->db->getCount($query);
			if($data['count'] == 0){
				verify_email($Data['email'],$Data['name'],$Data['token'],$Data['type']);
				$add = $this->db->insert('tbl_buyers' ,$Data);

				if($add){
					$success[] =  "Please check your email and click on the link provided to verify your account.";
					$data['success'] = $success;
				}else{
					$error[] =  "Something Wrong,Please Contact System Administrator";
					$data['error'] = $error;
				}
			}else{
				$error[] =  "Email is already exists";
				$data['error'] = $error;
			}	 
	  	}
	  	$data['country'] = country();
	  	$this->view('buyers/main/header');
		$this->view('buyers/register',$data);
		$this->view('buyers/main/footer');
	}

	public function verify_email(){
		 
		isset($this->url[2]) ? '':redirect('/');
		$Data 		= array('active' => 1);
		$condition 	= array('token' => $this->url[2] );
		$edit = $this->db->update('tbl_buyers',$Data,$condition);
		if($edit){
			$success[] =  "Verify successfully you can now login!";
			$data['success'] = $success;
				$this->view('buyers/main/header');
				$this->view('buyers/login',$data);
				$this->view('buyers/main/footer');
		} 

	}

	public function update_profile(){
		 	 
		is_buyer();
 		is_loggedin();
		$id = $_SESSION[ID];
		$buyers 		 = "SELECT * FROM tbl_buyers WHERE id='".$id."'";
 		$data['buyer']	 = $this->db->getFetch($buyers);
		$data['buyers']  = $this->db->getCount($buyers);
		$data['country'] = country();
		$data['buyers']  == true? '':redirect('buyers/index');
			
		if (isset($_POST['submit']) )  {
		// -------------------------------------------------------------
			if(empty($_POST['newpassword'])){
				$newpassword = $data['buyer']['password'];				
			}else{
				$newpassword = md5($_POST['newpassword']);
			}
			$confirmpassword = md5($_POST['confirmpassword']);
			$check = $this->db->getCount("SELECT * FROM tbl_buyers WHERE id ='". $id."' AND password ='".$confirmpassword."' ");

		if($check>0){
			$Data = array(
				'name' 			=> $_POST['name'],
				'email' 			=> $_POST['email'],
				'password'		=> $newpassword,
				'location'		=> $_POST['location']
				// 'contact_number'=> $_POST['contact_number'] 
			);
			$edit = $this->db->update('tbl_buyers',$Data,array('id' => $id ));

			if($edit){
				$success[] =  "Save Succesfully!";
				$data['success'] = $success;
 				$buyers 		 = "SELECT * FROM tbl_buyers WHERE id='".$id."'";
 				$data['buyer']	 = $this->db->getFetch($buyers);
				$_SESSION[NAME] =  ucwords($data['buyer']['name']);
				$_SESSION[EMAIL] =  strtolower($data['buyer']['email']);
			}else{
				$error[] =  "Something Wrong,Please Contact System Administrator";
				$data['error'] = $error;
			}	
		}else{
			$error[] =  "Wrong Current Password";
			$data['error'] = $error;
		}

		// -------------------------------------------------------------
		}

		$this->view('buyers/header',$data);
		$this->view('buyers/update_profile',$data);
		$this->view('buyers/footer');
	}

	public function forgot_password(){
		 	 
		 
		$data['title'] = 'Forgot Password';
		if(isset($_POST['submit'])){
			$table 			= table($this->url[0]);
			$query 			= "SELECT id,email,name,type FROM tbl_buyers WHERE  email ='". $_POST['email']."' ";
 			$data['user']	= $this->db->getFetch($query);
			$data['count']	= $this->db->getCount($query);
			reset_password($data['user']);
			if($data['count'] > 0){
				$success[] 		=  "Please check your email and click on the link provided to reset your password.";
				$data['success']= $success;
				$Data 			= array('date_forgot_password' => date("Y-m-d H:i:s"));
				$condition 		= array('id' =>$data['user']['id'] );
				$edit 			= $this->db->update('tbl_buyers',$Data,$condition);
			}else{
				$error[] =  "Invalid email";
				$data['error'] = $error;
			}
		}
		$this->view('buyers/main/header');
		$this->view('buyers/forgot_password',$data);
		$this->view('buyers/main/footer');
		 
	}

	public function reset_password(){
		
		$data['title'] = 'Reset Password';
		$id = d($this->url[2]);
		$members 		= "SELECT * FROM tbl_buyers WHERE id='".$id."' ";
 		$data['members']= $this->db->getCount($members);
		$data['member']	= $this->db->getFetch($members);

		if($data['member']['date_forgot_password']=='0000-00-00 00:00:00'){
			redirect('buyers/login');
		}


		if(isset($_POST['submit'])){
			if($_POST['new_password']==$_POST['confirm_password']){
				$edit = $this->db->update('tbl_buyers',array('password' => md5($_POST['new_password']),'date_forgot_password' => '0000-00-00 00:00:00' ),array('id'=>$id));
				if($edit){
					$success[] =  "Save Succesfully!";
					$data['success'] = $success;
				}
			}else{
					$error[] =  "New and Current Password did not match";
					$data['error'] = $error;
			}	
		}
		$this->view('buyers/main/header');
		$this->view('buyers/reset_password',$data);
		$this->view('buyers/main/footer');
	}

	public function expire(){

		isset($this->url[2]) ? '':redirect('buyers/index');
		$id = d($this->url[2]);
		$inquiries 		= "SELECT * FROM tbl_inquiries WHERE id='".$id."' ";
 		$data['inquiries']	= $this->db->getCount($inquiries);
		$data['inquiry']	= $this->db->getFetch($inquiries);
		$data['inquiries'] == true? '':redirect('buyers/index');
 		$close 	= $this->db->update('tbl_inquiries',array('status' => 'closed'),array('id' => $id ));
		if($close){
			// email_to_selected_member($data['quotation'],$data['member']);
			redirect('buyers/inquiry/'.e($data['inquiry']['id'])); 
		}
	}


}