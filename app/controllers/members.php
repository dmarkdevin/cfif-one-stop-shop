<?php
class Members extends Controller
{ 
	public function __construct(){
		$this->db 	= $this->model('db');
		$this->url 	= $this->url();		

	}

	public function index(){
 		is_member();
 		is_loggedin();
		$query 			= "SELECT * FROM tbl_quotation WHERE member_id='".$_SESSION[ID]."' ORDER BY date_added DESC LIMIT 10";
 		$data['quotations']	= $this->db->getQuery($query);
		$this->view('members/header',$data);
		$this->view('members/index',$data);
		$this->view('members/footer');
	}
 
	public function inquiry(){
	
		is_member();
 		is_loggedin();
		isset($this->url[2]) ? '':redirect('members/index');
		$id = d($this->url[2]);
		$quotation 			= "SELECT * FROM tbl_quotation WHERE id='".$id."' ";
 		$data['quotations']	= $this->db->getCount($quotation);
		$data['quotation']	= $this->db->getFetch($quotation);
		$data['quotations'] == true? '':redirect('members/index');
 		$inquiry 			= "SELECT * FROM tbl_inquiries WHERE id='".$data['quotation']['inquiries_id']."' ";
 		// $data['inquiry']	= $this->db->getCount($inquiry);
		$data['inquiry']	= $this->db->getFetch($inquiry);
 		$data['images'] 	= $this->db->getQuery("SELECT * FROM tbl_image  WHERE inquiries_id ='".$data['inquiry']['id']."' ") ;
		if(isset($_POST['send_quote'])) {
			$Data = array(
				'quote'			=> $_POST['quote'],
				'comment'		=> $_POST['comment'],
				'payment_type' => $_POST['payment_type'],
				'shipping_type' => $_POST['shipping_type']
			);
				$edit = $this->db->update('tbl_quotation' ,$Data,array('id' =>  $id));
			if($edit){
				redirect('members/inquiry/'.$this->url[2]);	 
			}
		}		
		$this->view('members/header',$data);
		$this->view('members/inquiry',$data);
		$this->view('members/footer');
	}
 
	public function inquiries(){

		is_member();
 		is_loggedin();
		$query 			= "SELECT * FROM tbl_quotation WHERE member_id='".$_SESSION[ID]."' ORDER BY date_added DESC ";
 		$data['quotations']	= $this->db->getQuery($query);
		$this->view('members/header',$data);
		$this->view('members/inquiries',$data);
		$this->view('members/footer');
	} 
 
	public function login(){
	
		if_login($this->url[0]);
		$data['title'] = 'LOGIN';
		if(isset($_POST['submit'])){
			$table = table($this->url[0]);
			$query = "SELECT email,password,id,name,type,email,active FROM tbl_members WHERE  email ='". $_POST['email']."' AND password ='".md5($_POST['password'])."' ";
 			$data['user']	= $this->db->getFetch($query);
			$data['count']	= $this->db->getCount($query);

			if($data['count'] > 0){
	 			if($data['user']['active']==1){
					$_SESSION[ID]    =  ($data['user']['id']);
					$_SESSION[NAME]  =  ucwords($data['user']['name']);
					$_SESSION[TYPE]  =  strtoupper($data['user']['type']);
					$_SESSION[EMAIL] =  strtolower($data['user']['email']);
					redirect('members/index');
				}else{
			 		$error[] =  "Your account is not yet active";
					$data['error'] = $error;
		 		}
			}else{
				$error[] =  "Invalid email or password.";
				$data['error'] = $error;
			}
		}
		$this->view('members/main/header');
		$this->view('members/login',$data);
		$this->view('members/main/footer');
	}

 	public function logout(){
		logout();
	}

	public function update_profile(){
		 	 
		is_member();
 		is_loggedin();
		$id = $_SESSION[ID];
		$members 		 = "SELECT * FROM tbl_members WHERE id='".$id."'";
 		$data['member']	 = $this->db->getFetch($members);
		$data['members'] = $this->db->getCount($members);
		$data['country'] = country();
		$materials 		 = "SELECT * FROM tbl_materials ";
 		$data['materials'] = $this->db->getQuery($materials);
		$data['members'] == true? '':redirect('members/index');
			
		if (isset($_POST['submit']) )  {
		// -------------------------------------------------------------
			if(empty($_POST['newpassword'])){
				$newpassword = $data['member']['password'];				
			}else{
				$newpassword = md5($_POST['newpassword']);
			}
			$confirmpassword = md5($_POST['confirmpassword']);
			$check = $this->db->getCount("SELECT * FROM tbl_members WHERE id ='". $id."' AND password ='".$confirmpassword."' ");

		if($check>0){
				$Data = array(
					'name' 			=> $_POST['name'],
					'email' 		=> $_POST['email'],
					'company_type' 	=> $_POST['company_type'],
					'shipping_type' => $_POST['shipping_type'],
					'lead_time' 	=> $_POST['lead_time'],
					'password'		=> $newpassword,
					// 'contact_number'=> $_POST['contact_number'] 
				);


			$edit   = $this->db->update('tbl_members',$Data,array('id' => $id ));
			$delete = $this->db->delete('tbl_members_material' ,array('member_id' => $id));

			// ------------------------------------------
			if(isset($_POST['materials'])):
			foreach ($_POST['materials'] as $materials) {
				$Data = array(
					'member_id'=> $data['member']['id'],
					'material_id'=> $materials
				);
				$add = $this->db->insert('tbl_members_material' ,$Data);
				// $edit = $this->db->update('tbl_image',$Data,array('id' => $image['id'] ));
			}
			endif;
			// ------------------------------------------

			if($edit){
				$success[] =  "Save Succesfully!";
				$data['success'] = $success;
 				$members 		 = "SELECT * FROM tbl_members WHERE id='".$id."'";
 				$data['member']	 = $this->db->getFetch($members);
				$_SESSION[NAME]  =  ucwords($data['member']['name']);
				$_SESSION[EMAIL] =  strtolower($data['member']['email']);
			}else{
				$error[] =  "Something Wrong,Please Contact System Administrator";
				$data['error'] = $error;
			}	
		}else{
			$error[] =  "Wrong Current Password";
			$data['error'] = $error;
		}

		// -------------------------------------------------------------

		}
		$this->view('members/header',$data);
		$this->view('members/update_profile',$data);
		$this->view('members/footer');
	}

	public function forgot_password(){
		 	 
		$data['title'] = 'Forgot Password';
		if(isset($_POST['submit'])){
			$table = table($this->url[0]);
			$query = "SELECT id,email,name,type FROM tbl_members WHERE  email ='". $_POST['email']."' ";
 			$data['user']	= $this->db->getFetch($query);
			$data['count']	= $this->db->getCount($query);
			reset_password($data['user']);
			if($data['count'] > 0){
				$success[] =  "Please check your email and click on the link provided to reset your password.";
				$data['success'] = $success;
				$Data 		= array('date_forgot_password' => date("Y-m-d H:i:s"));
				$condition 	= array('id' =>$data['user']['id'] );
				$edit = $this->db->update('tbl_members',$Data,$condition);
			}else{
				$error[] =  "Invalid email";
				$data['error'] = $error;
			}
		}
		$this->view('buyers/main/header');
		$this->view('members/forgot_password',$data);
		$this->view('buyers/main/footer'); 
	}

	public function reset_password(){
		$data['title'] = 'Reset Password';
		$id = d($this->url[2]);
		$members 		= "SELECT * FROM tbl_members WHERE id='".$id."' ";
 		$data['members']= $this->db->getCount($members);
		$data['member']	= $this->db->getFetch($members);
		if($data['member']['date_forgot_password']=='0000-00-00 00:00:00'){
			redirect('members/login');
		}

		if(isset($_POST['submit'])){
			if($_POST['new_password']==$_POST['confirm_password']){
				$edit = $this->db->update('tbl_members',array('password' => md5($_POST['new_password']),'date_forgot_password' => '0000-00-00 00:00:00' ),array('id'=>$id));
				if($edit){
					$success[] =  "Save Succesfully!";
					$data['success'] = $success;
				}
			}else{
					$error[] =  "New and Current Password did not match";
					$data['error'] = $error;
			}	
		}
		$this->view('members/main/header');
		$this->view('members/reset_password',$data);
		$this->view('members/main/footer');
	}


}