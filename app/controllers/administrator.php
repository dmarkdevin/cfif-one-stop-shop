<?php
class Administrator extends Controller
{
	public function __construct(){
		$this->db 	= $this->model('db');
		$this->url 	= $this->url();

	}

	public function index(){
		is_administrator();
		is_loggedin();
		$members 			= "SELECT * FROM tbl_members";
 		$data['list']		= $this->db->getQuery($members);
		$data['members']	= $this->db->getCount($members);
		$buyers 			= "SELECT * FROM tbl_buyers";
 		$data['buyers']		= $this->db->getCount($buyers);
		$inquiries 			= "SELECT * FROM tbl_inquiries";
 		$data['inquiries']	= $this->db->getCount($inquiries);
		$quotations 		= "SELECT * FROM tbl_quotation	";
 		$data['quotations']	= $this->db->getCount($quotations);
		$this->view('administrator/header',$data);
		$this->view('administrator/index',$data);
		$this->view('administrator/footer');
	}
	public function members(){

		is_administrator();
		is_loggedin();
		$query 			= "SELECT * FROM tbl_members";
 		$data['list']	= $this->db->getQuery($query);
		$data['count']	= $this->db->getCount($query);
		$this->view('administrator/header',$data);
		$this->view('administrator/members',$data);
		$this->view('administrator/footer');
	}
	public function buyers(){

		is_administrator();
		is_loggedin();
		$query 			= "SELECT * FROM tbl_buyers";
 		$data['list']	= $this->db->getQuery($query);
		$data['count']	= $this->db->getCount($query);
		$this->view('administrator/header',$data);
		$this->view('administrator/buyers',$data);
		$this->view('administrator/footer');
	}	
	public function inquiries(){

		is_administrator();
		is_loggedin();
		$query 			= "SELECT * FROM tbl_inquiries";
 		$data['list']	= $this->db->getQuery($query);
		$data['count']	= $this->db->getCount($query);
		$this->view('administrator/header',$data);
		$this->view('administrator/inquiries',$data);
		$this->view('administrator/footer');
	}	
	public function materials(){

		is_administrator();
		is_loggedin();
		$query 			= "SELECT * FROM tbl_materials";
 		$data['list']	= $this->db->getQuery($query);
		$data['count']	= $this->db->getCount($query);
		$this->view('administrator/header',$data);
		$this->view('administrator/materials',$data);
		$this->view('administrator/footer');
	}
	public function add_material(){

		is_administrator();
		is_loggedin();
		$data['member']	 = array();
		if (isset($_POST['submit']) )  {
			$Data = array('name' => $_POST['name']);
			$data['materials']	 = $this->db->getCount("SELECT * FROM tbl_materials WHERE name='".$Data['name']."'");
			if($data['materials'] == 0){
				$add = $this->db->insert('tbl_materials' ,$Data);
				if($add){
					$success[] =  "Save Succesfully!";
					$data['success'] = $success;
				}else{
					$error[] =  "Something Wrong,Please Contact System Administrator";
					$data['error'] = $error;
				}
			}else{
				$error[] =  "Material name is already exists";
				$data['error'] = $error;
			}
		}
		$this->view('administrator/header',$data);
		$this->view('administrator/materials/form',$data);
		$this->view('administrator/footer');
	}
	public function add_member(){

		is_administrator();
		is_loggedin();
		$data['member']	 	= array();
		$materials 		 	= "SELECT * FROM tbl_materials ";
 		$data['materials'] 	= $this->db->getQuery($materials);

		if (isset($_POST['submit']) )  {
			$Data = array(
				'capacity_rating' 	=> $_POST['capacity_rating'],
				'company_type'		=> $_POST['company_type'],
				'shipping_type'		=> $_POST['shipping_type'],
				'lead_time' 		=> $_POST['lead_time'],
				'name' 				=> $_POST['name'],
				'email' 			=> $_POST['email'],
				'password' 			=> md5($_POST['password']),
				'contact_number' 	=> $_POST['contact_number'],
				'location' 			=> $_POST['location'],
				'detail' 			=> $_POST['detail'],
				'type' 		=> "members",
				'active' 	=> 1,
				'token'		=> md5($_POST['email'].uniqid(rand()))
			);

			$data['members']	 = $this->db->getCount("SELECT * FROM tbl_members WHERE email='".$Data['email']."'");
 
			if($data['members'] == 0){
				// verify_email($Data['email'],$Data['name'],$Data['token'],$Data['type']);
				$add = $this->db->insert('tbl_members' ,$Data);
				$lastID = $this->db->conn->lastInsertId();

					if($lastID == 0){
						$lastID = 1;
					}

				foreach ($_POST['materials'] as $materials) {
					$Data = array(
						'member_id'=> $lastID,
						'material_id'=> $materials
					);
					$add_material = $this->db->insert('tbl_members_material' ,$Data);
					// $edit = $this->db->update('tbl_image',$Data,array('id' => $image['id'] ));
				}

				if($add){
					$success[] =  "Save Succesfully!";
					$data['success'] = $success;
				}else{
					$error[] =  "Something Wrong,Please Contact System Administrator";
					$data['error'] = $error;
				}
			}else{
				$error[] =  "Email is already exists";
				$data['error'] = $error;
			}
		}
		$this->view('administrator/header',$data);
		$this->view('administrator/members/form',$data);
		$this->view('administrator/footer');
	}
	public function update_material(){


		is_administrator();
		is_loggedin();
		isset($this->url[2]) ? '':redirect('administrator/materials');
		$id = d($this->url[2]);
 		$materials 		 	= "SELECT * FROM tbl_materials WHERE id='".$id."'";
 		$data['material']	= $this->db->getFetch($materials);
		$data['materials'] 	= $this->db->getCount($materials);
		$data['materials'] == true? '':redirect('administrator/materials');
		$newimage = '';

		if(isset($_POST['submit'])){
			$Data = array('name' => $_POST['name']);
			$edit = $this->db->update('tbl_materials' ,$Data, array('id' =>$id));
			if($edit){
				$success[] =  "Save Succesfully!";
				$data['success'] 	= $success;
				$data['material']	= $this->db->getFetch($materials);
			}else{
				$error[] 		=  "Something Wrong,Please Contact System Administrator";
				$data['error'] 	= $error;
			}
		}
		$this->view('administrator/header',$data);
		$this->view('administrator/materials/form',$data);
		$this->view('administrator/footer');
	}
	public function delete_material(){

		is_administrator();
		is_loggedin();
		isset($this->url[2]) ? '':redirect('administrator/materials');
		$id = d($this->url[2]);
 		$materials 		 	= "SELECT * FROM tbl_materials WHERE id='".$id."'";
 		$data['material']	= $this->db->getFetch($materials);
		$data['materials'] 	= $this->db->getCount($materials);
		$data['materials'] 	== true? '':redirect('administrator/materials'); 
		$delete =  $this->db->delete('tbl_materials' ,array('id' 	=> $id));
			if($delete){
				redirect('administrator/materials');		 
			}else{
				 
			}
	}
	public function update_member(){


		is_administrator();
		is_loggedin();
		isset($this->url[2]) ? '':redirect('members/lists');
		$id = d($this->url[2]);
 		$members 		 	= "SELECT * FROM tbl_members WHERE id='".$id."'";
 		$data['member']	 	= $this->db->getFetch($members);
		$data['members'] 	= $this->db->getCount($members);
		$materials 		 	= "SELECT * FROM tbl_materials ";
 		$data['materials'] 	= $this->db->getQuery($materials);
		$data['members'] 	== true? '':redirect('administrator/members');
		$newimage = '';

 		if(isset($_POST['submit'])){

		$password = empty($_POST['password']) ?  $data['member']['password'] : md5($_POST['password']);

			$Data = array(
				'capacity_rating' 	=> $_POST['capacity_rating'],
				'company_type'		=> $_POST['company_type'],
				'shipping_type'		=> $_POST['shipping_type'],
				'lead_time' 		=> $_POST['lead_time'],
				'password' 			=> $password,
				'name' 				=> $_POST['name'],
				'email' 			=> $_POST['email'],
				'contact_number' 	=> $_POST['contact_number'],
				'location' 			=> $_POST['location'],
				'detail' 			=> $_POST['detail']
				// 'image'				=> $newimage
				);


			$edit = $this->db->update('tbl_members' ,$Data, array('id' =>$id));

			$delete =  $this->db->delete('tbl_members_material' ,array('member_id' => $id));

			// ------------------------------------------
			if(isset($_POST['materials'])):
			foreach ($_POST['materials'] as $materials) {

				$Data = array(
					'member_id'=> $data['member']['id'],
					'material_id'=> $materials
				);

				$add_material = $this->db->insert('tbl_members_material' ,$Data);
			}
			endif;
			// ------------------------------------------


			if($edit){
				$success[] =  "Save Succesfully!";
				$data['success'] = $success;

				$data['member']	 = $this->db->getFetch($members);

			}else{
				$error[] =  "Something Wrong,Please Contact System Administrator";
				$data['error'] = $error;
			}
		} 
		$this->view('administrator/header',$data);
		$this->view('administrator/members/form',$data);
		$this->view('administrator/footer');
	}
	public function update_profile(){
		 	 
		is_administrator();
		is_loggedin();
		$id = $_SESSION[ID];

		$administrator 		 = "SELECT * FROM tbl_users WHERE id='".$id."'";
 		$data['administrator']	 = $this->db->getFetch($administrator);
		$data['administrators']  = $this->db->getCount($administrator);

		$data['country'] = country();

		$data['administrators'] == true? '':redirect('administrator/index');
			
		if (isset($_POST['submit']) )  {
			

		// ----------------------------------------------
			if(empty($_POST['newpassword'])){
				$newpassword = $data['administrator']['password'];				
			}else{
				$newpassword = md5($_POST['newpassword']);
			}
			$confirmpassword = md5($_POST['confirmpassword']);
			$check = $this->db->getCount("SELECT * FROM tbl_users WHERE id ='". $id."' AND password ='".$confirmpassword."' ");


		if($check>0){
		
				$Data = array(
					'name' 			=> $_POST['name'],
					'email' 		=> $_POST['email'],
					'password'		=> $newpassword,
				);
			$edit = $this->db->update('tbl_users',$Data,array('id' => $id ));

			if($edit){
				$success[] 				=  "Save Succesfully!";
				$data['success'] 		= $success;
 				$administrator 		 	= "SELECT * FROM tbl_users WHERE id='".$id."'";
 				$data['administrator']	= $this->db->getFetch($administrator);
				$_SESSION[NAME] 		= ucwords($data['administrator']['name']);
				$_SESSION[EMAIL] 		= strtolower($data['administrator']['email']);
			}else{
				$error[] =  "Something Wrong,Please Contact System Administrator";
				$data['error'] = $error;
			}	
		}else{
			$error[] =  "Wrong Current Password";
			$data['error'] = $error;
		}

		// -------------------------------------------------------------

		}
		$this->view('administrator/header',$data);
		$this->view('administrator/update_profile',$data);
		$this->view('administrator/footer');
	}	
	public function import(){
		is_administrator();
		is_loggedin();
		$data['title'] = "IMPORT ";
		if(isset($_POST['import'])){
			if($_FILES['file']['error']==0){
				$name = basename($_FILES['file']['name']);
				$size = $_FILES['file']['size'];
				$type = $_FILES['file']['type'];
				$ext = substr($name,strrpos($name,".")+1);
				if(($size/1024/1024)<1){
					if($type=="application/vnd.ms-excel"){
						if ( isset( $_FILES['file'] ) ){
						$csv_file = $_FILES['file']['tmp_name'];
							if ( ! is_file( $csv_file ) )
							exit('File not found.');
							$sql = '';
							if (($handle = fopen( $csv_file, "r")) !== FALSE){
							 	  $headers = fgetcsv($handle, 1000, ",");
								while (($datax = fgetcsv($handle, 1000, ",")) !== FALSE){
		 
 
						 		$sql = "SELECT * FROM tbl_members WHERE  name=:name ";
						        $query = $this->db->conn->prepare($sql);
						        $query->bindValue(':name', ($datax[0]), PDO::PARAM_STR);
						        $query->execute();
						        $variable = $query->rowCount();
						        $members = $query->fetch();


							  if( $variable == 0    ){
								$field = array(
									'active'=>1,
									'type'=>'members',
									'password'=>'21232f297a57a5a743894a0e4a801fc3',
									'company_type'=>'any',
									'shipping_type'=>'all',
									'capacity_rating'=>'100',
									'lead_time'=>'1',
									'name'=>($datax[0]),
									'location'=>($datax[1]),
									'contact_person'=>($datax[2]),
									'contact_number'=>($datax[3]),
									'email'=>($datax[4])
 


									);
												 
	 
													// echo $datax[0].'<br>';
							$add = $this->db->insert('tbl_members',$field);
							$lastID = $this->db->conn->lastInsertId();

							if($lastID == 0){
								$lastID = 1;
							}

  
							if(isset($datax[5])):

								$pieces = explode(',', $datax[5]);
							foreach($pieces as $element)
							{
							 	$sql = "SELECT * FROM tbl_materials WHERE  name=:name ";
						        $query = $this->db->conn->prepare($sql);
						        $query->bindValue(':name', ($element), PDO::PARAM_STR);
						        $query->execute();
						        $variable = $query->rowCount();
						        $materials = $query->fetch();


								if($variable > 0){
										$Data = array(
										'member_id'=> $lastID,
										'material_id'=> $materials['id']
										);
									$add_material = $this->db->insert('tbl_members_material' ,$Data);
								}else{
									
								}

							}
							endif;
							$errors[] = ("yyy");
							$data['errors'] = $errors;


 
						}else{
							$field = array(
								'active'=>1,
								'type'=>'members',
								'password'=>'21232f297a57a5a743894a0e4a801fc3',
								'company_type'=>'any',
								'shipping_type'=>'all',
								'capacity_rating'=>'100',
								'lead_time'=>'1',

								'name'=>($datax[0]),
								'location'=>($datax[1]),
								'contact_person'=>($datax[2]),
								'contact_number'=>($datax[3]),
								'email'=>($datax[4])
								 
								);

 
							$edit = $this->db->update('tbl_members',$field,array('id' => $members['id'] ));
							$delete =  $this->db->delete('tbl_members_material' ,array('member_id' => $members['id']));

							// ------------------------------------------
							if(isset($datax[5])):
							$pieces = explode(',', $datax[5]);
							foreach($pieces as $element)
							{

								$sql = "SELECT * FROM tbl_materials WHERE  name=:name ";
								$query = $this->db->conn->prepare($sql);
								$query->bindValue(':name', ($element), PDO::PARAM_STR);
								$query->execute();
								$variable = $query->rowCount();
								$materials = $query->fetch();


								if($variable > 0){
									$Data = array(
									'member_id'=> $members['id'],
									'material_id'=> $materials['id']
									);
									$add_material = $this->db->insert('tbl_members_material' ,$Data);
								 
								}else{
								 
								}


							}
							endif;
							// ------------------------------------------
							}

							// ==============================
								}
								// redirect('administrator/import');
								fclose($handle);
							}
						}
					}else{
						$errors[] = ("Invalid file");
						$data['errors'] = $errors;
					}
				}else{
					$errors[] = ("You reached file limit 1 mb");
					$data['errors'] = $errors;
				}
			}else{
				$errors[] = ("File upload error");
				$data['errors'] = $errors;
			}
		}



		$this->view('administrator/header',$data);
		$this->view('administrator/import_members',$data);
		$this->view('administrator/footer',$data);
	}
	public function login(){

		if_login($this->url[0]);

		$data['title'] = 'LOGIN';

		if(isset($_POST['submit'])){

			$table = table($this->url[0]);
			$query = "SELECT email,password,id,name,type,email FROM tbl_users WHERE  email ='". $_POST['email']."' AND password ='".md5($_POST['password'])."' ";

 			$data['user']	= $this->db->getFetch($query);
			$data['count']	= $this->db->getCount($query);

			if($data['count'] > 0){
					$_SESSION[ID]    =  ($data['user']['id']);
					$_SESSION[NAME]  =  ucwords($data['user']['name']);
					$_SESSION[TYPE]  =  strtoupper($data['user']['type']);
					$_SESSION[EMAIL] =  strtolower($data['user']['email']);
					redirect('administrator/index');
			}else{
				$error[] =  "Invalid email or password.";
				$data['error'] = $error;
			}
		}
		$this->view('administrator/main/header');
		$this->view('administrator/login',$data);
		$this->view('administrator/main/footer');	
	}
	public function logout(){
		logout();
	}
	public function forgot_password(){
		 	 
		$data['title'] = 'Forgot Password';
		$this->view('administrator/main/header');
		$this->view('administrator/forgot_password',$data);
		$this->view('administrator/main/footer');	 
	}
}
