<?php
class Main extends Controller
{ 
	public function __construct(){
		$this->db 	= $this->model('db');
		$this->url 	= $this->url();		

	}

	public function index(){
	
		$this->view('main/header');
		$this->view('main/index',$data='');
		$this->view('main/footer');

	}
	public function page2(){
	
		$this->view('main/header');
		$this->view('main/page2',$data='');
		$this->view('main/footer');

	}
	public function page3(){
	
		$this->view('main/header');
		$this->view('page/profile',$data='');
		$this->view('main/footer');

	}
	public function page4(){
	
		$this->view('main/header');
		$this->view('page/login',$data='');
		$this->view('main/footer');

	}
}