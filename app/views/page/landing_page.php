
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ONE STOP SHOP</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=ASSETS.FRONT;?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=ASSETS.FRONT;?>css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=ASSETS.FRONT;?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
      <link rel="Shortcut Icon" type="image/ico" href="<?=ASSETS;?>images/favicon.ico" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation" style="background-color: #FFF; " style="padding:20px;">
        <div class="container topnav" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
       <div style="margin:20px;">
            <img src="<?=ASSETS;?>images/cfif.png" height="80px;"  class="logo"> 
       </div>         
               



            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->


<style type="text/css">
    .nav a {
        font-family: Century Gothic;
        font-size: 14px;
    }
</style>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
                <ul class="nav navbar-nav navbar-right" style="margin:20px;padding-top:18px; ; ">
                    <li >
                        <a style="color: #FD550C;" href="https://www.cebufurnitureindustries.com" target="_blank">CFIF HOME</a>
                    </li>
                    <li>
                        <a style="color: #FD550C;" href="#about">ABOUT ONE STOP SHOP</a>
                    </li>
                    <li>
                        <a style="color: #FD550C;" href="<?=URL_ROOT;?>members/login">MEMBER LOGIN</a>
                    </li>
                    <li>
                        <a style="color: #FD550C;" href="#contact">CONTACT</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


    <!-- Header -->
    <a name="about"></a>
    <div class="intro-header">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h3>CFIF | ONE STOP SHOP</h3> 
                        <h1>Find the perfect match for you <br>- in the Queen city of the South</h1>
                        <!-- <hr class="intro-divider"> -->
                        <ul class="list-inline intro-social-buttons">
                        <li>


<a href="<?=URL_ROOT;?>buyers/login" class="btn btn-default btn-lg sign-in"   >
<!-- <i class="fa fa-user "></i>  -->
<span class="network-name">SIGN IN HERE</span>

</a>

 


                        </li>
                            <!-- <li>
                                <a href="<?=URL_ROOT;?>members/login" class="btn btn-default btn-lg"><i class="fa fa-user fa-fw"></i> <span class="network-name">MEMBER LOGIN</span></a>
                            </li> -->
<!--                             <li>
                                <a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                            </li> -->
                        </ul>
<p class="text" >Don't have an account? <a href="<?=URL_ROOT;?>buyers/register">Sign up today!</a>
<br>Membership company? <a href="<?=URL_ROOT;?>members/login">Sign in here</a>

</p>




                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->
    <div class="content-section-a">

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                     
                </div>
            </div>

        </div>
 

    </div> 
 	<a  id="about"></a>
    <div class="content-section-a">

        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12" >
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading" style="text-align: center;">What is the CFIF One Stop Shop?</h2>
                    <p class="lead" style="text-align: left;">One Stop Shop, or One Stop Inquiry Hub is a CFIF initiative and system that has been developed to match local and international customers to the right furniture manufacturer in Cebu, Philippines. </p>
                    <p class="lead" style="text-align: left;">With the 'right' we mean the companies which has the capacity, skill and means to cater your order based on your submitted inquiry. It works like a match making site, but for customers and factories. Try it out today!</p>
                </div>
                <!-- <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="<?=ASSETS.FRONT;?>img/ipad.png" alt="">
                </div> -->
            </div>

        </div>
 

    </div>  
    <!-- /.content-section-a -->
 <!--    <div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">3D Device Mockups<br>by PSDCovers</h2>
                    <p class="lead">Turn your 2D designs into high quality, 3D product shots in seconds using free Photoshop actions by <a target="_blank" href="http://www.psdcovers.com/">PSDCovers</a>! Visit their website to download some of their awesome, free photoshop actions!</p>
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="<?=ASSETS.FRONT;?>img/dog.png" alt="">
                </div>
            </div>

        </div>
        

    </div>  -->
    <!-- /.content-section-b -->

  <!--  <div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Google Web Fonts and<br>Font Awesome Icons</h2>
                    <p class="lead">This template features the 'Lato' font, part of the <a target="_blank" href="http://www.google.com/fonts">Google Web Font library</a>, as well as <a target="_blank" href="http://fontawesome.io">icons from Font Awesome</a>.</p>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="<?=ASSETS.FRONT;?>img/phones.png" alt="">
                </div>
            </div>

        </div>
        

    </div> -->
    <!-- /.content-section-a -->

<!--  	<a  name="contact"></a>
    <div class="banner">

        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2>Connect to Start Bootstrap:</h2>
                </div>
                <div class="col-lg-6">
                    <ul class="list-inline banner-social-buttons">
                        <li>
                                <a href="<?=URL_ROOT;?>buyers/login" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">BUYERS LOGIN</span></a>
                            </li>
                            <li>
                                <a href="<?=URL_ROOT;?>members/login" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">MEMBERS LOGIN</span></a>
                            </li>
                    </ul>
                </div>
            </div>

        </div>
        

    </div>  -->
    <!-- /.banner -->

    <!-- Footer -->
    <footer style="background: #FD550C;text-align: center;" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">


<img src="<?=ASSETS;?>images/logo-white.png" style='height:160px;margin-bottom:30px;' >
 

<div class="text1">Copyright © 2017 Cebu Furniture Industries Foundation, Inc. All Rights Reserved</div>
<div class="text2">Tel. No: (63-32) 420 – 4143   |    Fax No: (63-32) 420 – 4160    |    Email: info@furniturecebu.com</div>
<div class="text1">Unit 15 Green Strips Bldg., S.E. Jayme St., Pak-naan, Mandaue City 6014 Cebu Philippines</div>


<style type="text/css">
    .social-icons{
        margin-top: 20px;
    }
    .social-icons a{
         margin-right: 15px !important;
        
        font-size: 20px;
        color: #fff;
    }
</style>

<div class="social-icons" >
<a href="https://www.linkedin.com/company-beta/2235569/" target="_blank"><i class="fa fa-linkedin" ></i></a>
<a href="http://instagram.com/cebufurnitureindustries" target="_blank"><i class="fa fa-instagram"></i></a>
<a href="https://www.facebook.com/cebufurniture" target="_blank"><i class="fa fa-facebook"></i></a>
<a href="" target="_blank" mailto=""  ><i class="fa fa-envelope"></i></a>
</div>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="<?=ASSETS.FRONT;?>js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=ASSETS.FRONT;?>js/bootstrap.min.js"></script>

</body>

</html>
