
<div class="row">
  <div class="col-xs-12">


    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Materials</h3>
        <?=add_button('administrator/add_material','Add Materials');?>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="table-responsive col-sm-12">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>Number</th>
            <th>Name</th>
         
            <th></th>
            
             
          </tr>
          </thead>
          <tbody>
          <?php foreach ($data['list'] as $key => $value) { ?>
          <tr>

            <td><?=($value['id']);?></td>
            <td><?=strtolower($value['name']);?></td>
            
            <td>
              <?=update_button('administrator/update_material',$value['id']);?>
              <?=delete_button('administrator/delete_material',$value['id']);?>
            </td>           
          </tr>
          <?php } ?> 
          </tbody>
         
        </table>
        </div>
      </div>
      <!-- /.box-body -->
          <!--   <div class="box-footer clearfix">
              <p  class="pull-right"><a href="<?=URL_ROOT;?>buyers/inquiries">View All Transactions</a> </p>
            </div> -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
      

 