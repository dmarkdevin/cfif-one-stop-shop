<!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?=URL_ROOT;?>buyers/inquire2" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label >Quantity *</label>
                  <input type="text" class="form-control" name="quantity">
                </div>
                <div class="form-group">
                  <label >Price Per Unit (US Dollar) *</label>
                  <input type="number" class="form-control" name="">
                </div>
                <div class="form-group">
                  <label >Materials *</label>
                  <input type="number" class="form-control" name="">
                </div>
                <div class="form-group">
                  <label >Lead Time *</label>
                  <input type="number" class="form-control" name="">
                </div>
                <div class="form-group">
                  <label >Type *</label>
                  <input type="number" class="form-control" name="">
                </div>
                 <div class="form-group">
                  <label>Description *</label>
                  <textarea class="form-control" rows="2" name=""></textarea>
                </div>

                <div class="form-group">
                  <label>Images / Drawing *</label>
                  <input type="file" name="">

                  <p class="help-block">Image file only.</p>
                </div>
 
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">SEND INQUIRY</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
