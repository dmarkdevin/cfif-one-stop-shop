 

 

  <div class="row">
    <div class="col-xs-12">
     
        <?php
          if(!empty($data['error'])){
            error_message($data['error']);
          }
          if(!empty($data['success']) ){
            success_message($data['success']);
          }
        ?>   
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Materials</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body ">


 

        <form  method="post" action="" role="form" enctype="multipart/form-data" class="form-horizontal">

          <div class="form-group">
            <label class="col-sm-2 control-label">Material Name * </label>
            <div class="col-sm-10">
              <input type="text" name="name" class="form-control" required placeholder="Material Name" value="<?=isset($data['material']['name']) ? $data['material']['name'] : ''; ?>" autofocus>
              
            </div>
          </div>

        

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
               
              <input type="submit" name="submit" class="btn btn-default"  value="Save">
            </div>
          </div>
        </form>
      </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
 