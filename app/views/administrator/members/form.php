 

 

  <div class="row">
    <div class="col-xs-12">
     
          <?php
          if(!empty($data['error'])){
            error_message($data['error']);
          }
          if(!empty($data['success']) ){
            success_message($data['success']);
          }
        ?> 
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Members Registration</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body ">


 
 

        <form  method="post" action=""  enctype="multipart/form-data" class="form-horizontal">

          <div class="form-group">
            <label class="col-sm-2 control-label">Name * </label>
            <div class="col-sm-10">
              <input type="text" name="name" class="form-control" required placeholder="Full Name" value="<?=isset($data['member']['name']) ? $data['member']['name'] : ''; ?>">

 

              
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Email * </label>
            <div class="col-sm-10">
              <input type="email" name="email" class="form-control" required placeholder="Email Address" value="<?=isset($data['member']['email']) ? $data['member']['email'] : ''; ?>">
              
            </div>
          </div>  

          <div class="form-group">
            <label class="col-sm-2 control-label">Password * </label>
            <div class="col-sm-10">
              <input type="password" name="password" class="form-control"  placeholder="Password">
              
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Retype Password * </label>
            <div class="col-sm-10">
              <input type="password" name="confirm_password" class="form-control"  placeholder="Retype Password">
              
            </div>
          </div>


          <div class="form-group">
          <label class="col-sm-2 control-label">Logo</label>
          <div class="col-sm-5">
          <input type="file"  name="image">

          <p class="help-block">Square, Image file only, recommended size: 128x128px.<br>Upload new image only if you want to change.</p>
          </div>
          <div class="col-sm-5">
          </div>
        </div>
 
          <hr>
          <div class="form-group">
            <label class="col-sm-2 control-label">Location * </label>
            <div class="col-sm-10">
              <input type="text" name="location" class="form-control"  placeholder="Location" value="<?=isset($data['member']['location']) ? $data['member']['location'] : ''; ?>"  >
            </div>
          </div> 
          <div class="form-group">
            <label class="col-sm-2 control-label">Contact Number * </label>
            <div class="col-sm-10">
              <input type="text" name="contact_number" class="form-control" placeholder="Contact Number"  value="<?=isset($data['member']['contact_number']) ? $data['member']['contact_number'] : ''; ?>" >
            </div>
          </div> 
           
          <div class="form-group">

             <label class="col-sm-2 control-label">Detail * </label>
            <div class="col-sm-10">
            <textarea name="detail"   class="textarea" placeholder="Detail" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" ><?=isset($data['member']['detail']) ? $data['member']['detail'] : ''; ?></textarea>
            </div>
          </div>

          <hr>
          <div class="form-group">
            <label class="col-sm-2 control-label">Capacity Rating * </label>
            <div class="col-sm-10">
              <input type="number" name="capacity_rating" class="form-control"  placeholder="Capacity Rating" value="<?=isset($data['member']['capacity_rating']) ? $data['member']['capacity_rating'] : ''; ?>">
            </div>
          </div> 


          <div class="form-group" >
            <label class="col-sm-2 control-label">Factory Type</label>
            <div class="col-sm-10">
              <select class="form-control" name="company_type"  required>
                <option <?=isset($data['member']['company_type']) && ($data['member']['company_type']=='') ?'selected' : ''; ?> value=""></option>
                <option <?=isset($data['member']['company_type']) && ($data['member']['company_type']=='residential') ? 'selected' : ''; ?> value="residential">Residential</option>
                <option <?=isset($data['member']['company_type']) && ($data['member']['company_type']=='commercial') ? 'selected' : ''; ?> value="commercial">Commercial</option>
 
                <option <?=isset($data['member']['company_type']) && ($data['member']['company_type']=='any') ? 'selected' : ''; ?> value="any">Any</option>
              </select>
            </div>
          </div> 

          <div class="form-group" >
            <label class="col-sm-2 control-label">Shipping Type</label>
            <div class="col-sm-10">
              <select class="form-control" name="shipping_type" required >
                <option <?=isset($data['member']['shipping_type']) && ($data['member']['shipping_type']=='') ? 'selected' : ''; ?> value=""></option>
                <option <?=isset($data['member']['shipping_type']) && ($data['member']['shipping_type']=='all') ? 'selected' : ''; ?> value="all">All</option>
                <option <?=isset($data['member']['shipping_type']) && ($data['member']['shipping_type']=='export') ? 'selected' : ''; ?> value="export">Export only</option>
                <option <?=isset($data['member']['shipping_type']) && ($data['member']['shipping_type']=='domestic') ? 'selected' : ''; ?> value="domestic">Domestic Only</option>
               </select>
            </div>
          </div> 

          <div class="form-group" >
            <label class="col-sm-2 control-label">Lead Time</label>
            <div class="col-sm-10">
              <select class="form-control" name="lead_time"  required>
                <option <?=isset($data['member']['lead_time']) && ($data['member']['lead_time']==0) ? 'selected' : ''; ?> value="0">Lead Time</option>
                <option <?=isset($data['member']['lead_time']) && ($data['member']['lead_time']==1) ? 'selected' : ''; ?> value="1">LESS THAN 30 DAYS</option>
                <option <?=isset($data['member']['lead_time']) && ($data['member']['lead_time']==2) ? 'selected' : ''; ?> value="2">30-60 DAYS</option>
                <option <?=isset($data['member']['lead_time']) && ($data['member']['lead_time']==3) ? 'selected' : ''; ?> value="3">60-90 DAYS</option>
                <option <?=isset($data['member']['lead_time']) && ($data['member']['lead_time']==4) ? 'selected' : ''; ?> value="4">OVER 90 DAYS</option>
              </select>
            </div>
          </div> 
<!--
            <div class="form-group" >
            <label class="col-sm-2 control-label">Material Type 1</label>
            <div class="col-sm-10">
              <select class="form-control" name="material_type1"  >
                <option <?=isset($data['member']['material_type1']) && ($data['member']['material_type1']=="") ? 'selected' : ''; ?> value=""></option>
                <option <?=isset($data['member']['material_type1']) && ($data['member']['material_type1']=="wood") ? 'selected' : ''; ?> value="wood">Wood</option>
                <option <?=isset($data['member']['material_type1']) && ($data['member']['material_type1']=="rattan") ? 'selected' : ''; ?> value="rattan">Rattan</option>
                <option <?=isset($data['member']['material_type1']) && ($data['member']['material_type1']=="metal") ? 'selected' : ''; ?> value="metal">Metal</option>
               
              </select>
            </div>
          </div> 
            <div class="form-group" >
            <label class="col-sm-2 control-label">Material Type 2</label>
            <div class="col-sm-10">
              <select class="form-control" name="material_type2"  >
                <option <?=isset($data['member']['material_type2']) && ($data['member']['material_type2']=="") ? 'selected' : ''; ?> value=""></option>
                <option <?=isset($data['member']['material_type2']) && ($data['member']['material_type2']=="wood") ? 'selected' : ''; ?> value="wood">Wood</option>
                <option <?=isset($data['member']['material_type2']) && ($data['member']['material_type2']=="rattan") ? 'selected' : ''; ?> value="rattan">Rattan</option>
                <option <?=isset($data['member']['material_type2']) && ($data['member']['material_type2']=="metal") ? 'selected' : ''; ?> value="metal">Metal</option>
               
              </select>
            </div>
          </div> 
            <div class="form-group" >
            <label class="col-sm-2 control-label">Material Type 3</label>
            <div class="col-sm-10">
              <select class="form-control" name="material_type3"  >
                <option <?=isset($data['member']['material_type3']) && ($data['member']['material_type3']=="") ? 'selected' : ''; ?> value=""></option>
                <option <?=isset($data['member']['material_type3']) && ($data['member']['material_type3']=="wood") ? 'selected' : ''; ?> value="wood">Wood</option>
                <option <?=isset($data['member']['material_type3']) && ($data['member']['material_type3']=="rattan") ? 'selected' : ''; ?> value="rattan">Rattan</option>
                <option <?=isset($data['member']['material_type3']) && ($data['member']['material_type3']=="metal") ? 'selected' : ''; ?> value="metal">Metal</option>
               
              </select>
            </div>
          </div>  -->
          <div class="form-group">
            <label class="col-sm-2 control-label">Materials</label>
            <div class="col-sm-offset-2 col-sm-10">
              <div class="row">
                <?php
                
                foreach ($data['materials'] as $key => $value) {
                
                if(isset($data['member']['id'])){
                  $query = "SELECT * FROM tbl_members_material WHERE material_id='".$value['id']."' AND member_id='".$data['member']['id']."' ";  
                          
                  $data['count']  = $this->db->getCount($query);                 

                  $checked = ($data['count'] > 0) ? 'checked':'';

               }else{
                  $checked = '';
               }
 
                ?>


                <div class="col-sm-2">
                  <div class="checkbox">
                    <label>
                    <input type="checkbox" name="materials[]"  value="<?=$value['id'];?>" <?=$checked;?>   > 
                     <?=$value['name'];?>
                    </label>
                  </div>
                </div>     
                <?php
                }
                ?>
                
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
               
             

              <button type="submit" name="submit" class="btn btn-default"><i class="fa fa-fw fa-save"></i> SAVE</button>
            </div>
          </div>
        </form>
      </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
 