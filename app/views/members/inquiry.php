

      <div class="row">
        <div class="col-md-4"><?=$this->view('buyers/inquiry/inquiry_left',$data);?></div>
        
        <!-- /.col -->
        <div class="col-md-8">
          
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"> </h3>

                <span class="mailbox-read-time pull-right"><?=$data['inquiry']['date_added'];?></span>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
 
 
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                <p>Hello <?=name();?>,</p>
                <p><?=$data['inquiry']['additional_info'];?></p>

              
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                  
            <?php  

            if(!isset($data['quotation']['quote'])) {
             
                  if(($data['inquiry']['status']!='closed' )) {
                    $days = days($data['inquiry']['date_added']);
                    if($days>3) {
                      $this->view('members/inquiry/if_closed',$data);
                      // echo days($data['inquiry']['date_added']);
                    }else{
                      
                      $this->view('members/inquiry/quotation_form');
                    }
                    
                  }else{ 
                    $this->view('members/inquiry/if_closed',$data);
                  } 
            }else{ 
              $this->view('members/inquiry/if_submitted',$data);
             
            } 

            ?>         


            </div>
 
          </div>
          <!-- /. box -->
        
 
        

 
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    