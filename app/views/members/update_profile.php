 

 

  <div class="row">
    <div class="col-xs-12">
        <?php
          if(!empty($data['error'])){
            error_message($data['error']);
          }
          if(!empty($data['success']) ){
            success_message($data['success']);
          }
        ?>   
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Profile</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body ">

        <form class="form-horizontal" method="post" action="" role="form" enctype="multipart/form-data">

          <div class="form-group">
            <label class="col-sm-2 control-label">Full Name * </label>
            <div class="col-sm-10">
              <input type="text" name="name" class="form-control" required placeholder="Full Name" value="<?=isset($data['member']['name']) ? $data['member']['name'] : ''; ?>" readonly>
              
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Email * </label>
            <div class="col-sm-10">
              <input type="email" name="email" class="form-control" required placeholder="Email Address" value="<?=isset($data['member']['email']) ? $data['member']['email'] : ''; ?>" readonly>
              
            </div>
          </div>  

           <div class="form-group" >
            <label class="col-sm-2 control-label">Factory Type</label>
            <div class="col-sm-10">
              <select class="form-control" name="company_type"  required>
                <option <?=isset($data['member']['company_type']) && ($data['member']['company_type']=='') ?'selected' : ''; ?> value=""></option>
                <option <?=isset($data['member']['company_type']) && ($data['member']['company_type']=='residential') ? 'selected' : ''; ?> value="residential">Residential</option>
                <option <?=isset($data['member']['company_type']) && ($data['member']['company_type']=='commercial') ? 'selected' : ''; ?> value="commercial">Commercial</option>
 
                <option <?=isset($data['member']['company_type']) && ($data['member']['company_type']=='any') ? 'selected' : ''; ?> value="any">Any</option>
              </select>
            </div>
          </div> 

          <div class="form-group" >
            <label class="col-sm-2 control-label">Shipping Type</label>
            <div class="col-sm-10">
              <select class="form-control" name="shipping_type" required >
                <option <?=isset($data['member']['shipping_type']) && ($data['member']['shipping_type']=='') ? 'selected' : ''; ?> value=""></option>
                <option <?=isset($data['member']['shipping_type']) && ($data['member']['shipping_type']=='all') ? 'selected' : ''; ?> value="all">All</option>
                <option <?=isset($data['member']['shipping_type']) && ($data['member']['shipping_type']=='export') ? 'selected' : ''; ?> value="export">Export only</option>
                <option <?=isset($data['member']['shipping_type']) && ($data['member']['shipping_type']=='domestic') ? 'selected' : ''; ?> value="domestic">Domestic Only</option>
               </select>
            </div>
          </div> 
          <div class="form-group" >
            <label class="col-sm-2 control-label">Lead Time</label>
            <div class="col-sm-10">
              <select class="form-control" name="lead_time"  required>
                <option <?=isset($data['member']['lead_time']) && ($data['member']['lead_time']==0) ? 'selected' : ''; ?> value="0">Lead Time</option>
                <option <?=isset($data['member']['lead_time']) && ($data['member']['lead_time']==1) ? 'selected' : ''; ?> value="1">LESS THAN 30 DAYS</option>
                <option <?=isset($data['member']['lead_time']) && ($data['member']['lead_time']==2) ? 'selected' : ''; ?> value="2">30-60 DAYS</option>
                <option <?=isset($data['member']['lead_time']) && ($data['member']['lead_time']==3) ? 'selected' : ''; ?> value="3">60-90 DAYS</option>
                <option <?=isset($data['member']['lead_time']) && ($data['member']['lead_time']==4) ? 'selected' : ''; ?> value="4">OVER 90 DAYS</option>
              </select>
            </div>
          </div> 
          <div class="form-group">
            <label class="col-sm-2 control-label">Materials</label>
            <div class="col-sm-offset-2 col-sm-10">
              <div class="row">
                <?php
                
                foreach ($data['materials'] as $key => $value) {
                
                if(isset($data['member']['id'])){
                  $query = "SELECT * FROM tbl_members_material WHERE material_id='".$value['id']."' AND member_id='".$data['member']['id']."' ";  
                          
                  $data['count']  = $this->db->getCount($query);                 

                  $checked = ($data['count'] > 0) ? 'checked':'';

               }else{
                  $checked = '';
               }
 
                ?>


                <div class="col-sm-2">
                  <div class="checkbox">
                    <label>
                    <input type="checkbox" name="materials[]"  value="<?=$value['id'];?>" <?=$checked;?>   > 
                     <?=$value['name'];?>
                    </label>
                  </div>
                </div>     
                <?php
                }
                ?>
                
              </div>
            </div>
          </div>

           <div class="form-group">
            <label class="col-sm-2 control-label">Change Password * </label>
            <div class="col-sm-10">
              <input type="password" name="newpassword" class="form-control"   placeholder="Enter only if you want to change the password" value="">
               
              
            </div>
          </div>  

           <div class="form-group">
            <label class="col-sm-2 control-label">Confirm Password * </label>
            <div class="col-sm-10">
              <input type="password" name="confirmpassword" class="form-control" required placeholder="Confirm Password" value="">
              <p class="help-block">Please confirm your current password to make changes to your profile.</p>
              
            </div>
          </div>          
      


          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
               
              <button type="submit" name="submit" class="btn btn-default" > <i class="fa fa-save"></i> Save</button>
            </div>
          </div>
        </form>
      </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
 