
<div class="row">
  <div class="col-xs-12">


    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Inquiries</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="table-responsive col-sm-12">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th></th>
            <th>Inquiry #</th>
            <th>Qty</th>
            <th>Price Per Unit</th>
            <th>Lead Time</th>
            <th>Material</th>
            <th>Description</th>
            <th>Status</th>
            
             
          </tr>
          </thead>
          <tbody>
          <?php foreach ($data['quotations'] as $key => $value) { 
 

          $inquiries     = "SELECT * FROM tbl_inquiries WHERE id='".$value['inquiries_id']."' ";
          $data['inquiries'] = $this->db->getFetch($inquiries);

                $days = days($data['inquiries']['date_added']);

                if($days > 3){
                  $status = inquiry_status('closed');
                }else{
                  $status =  inquiry_status($data['inquiries']['status']);
                }

            ?>
          <tr>
            <td><a class="btn btn-default btn-flat btn-xs" title="" tooltip href="<?=URL_ROOT;?>members/inquiry/<?=e($value['id']);?>"><i class="fa fa-info"></i> Preview</a></td>
       
            <td><?=($data['inquiries']['id']);?></td>
            <td><?=($data['inquiries']['volume']);?></td>
            <td>USD <?=($data['inquiries']['target_price']);?></td>
            <td><?=lead_time($data['inquiries']['lead_time']);?></td>
            <td><?=ucwords($data['inquiries']['material']);?></td>
            <td><?=readmore($data['inquiries']['additional_info']);?></td>
            <td>

            <?php 

              if($value['status']=='selected'){ 
                echo selected();
              }else{ 
                echo (isset($value['quote'])) ? submitted() :  $status;
              
              } ?>
                    
            </td>
            
          </tr>
          <?php } ?> 
          </tbody>
         
        </table>
        </div>
      </div>
      <!-- /.box-body -->
         
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
      

 