                  <form action="" method="POST" >
                  
                  <div class="form-group">
                          <label >Comment *</label>
                          <textarea name="comment" class="form-control input-sm" required class="textarea" placeholder="Comment" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
                  <div class="form-group">
                    <label >Price Per Unit (US Dollar) *</label>
                    <input type="number" class="form-control " name="quote" value=" " placeholder="Price per unit" min="1" required>
                  </div>
                  <div class="form-group">
                    <label >Payment Type *</label>
                    <select class="form-control" name="payment_type" style="width: 100%;" required>
                      

                        <option   value="" ></option>
                        <option   value="C">Credit</option>
                        <option   value="DP">Down Payment</option>
                        <option   value="FP">Full Payment</option>
                       
                       
                    </select>
                  </div>
                  <div class="form-group">
                    <label >Shipping Type *</label>
                    <select class="form-control" name="shipping_type" style="width: 100%;" required>
                      

                        <option   value="" ></option>
                        <option   value="fob_cebu">FOB Cebu</option>
                        <option   value="door_to_door">Door to door </option>
                         
                       
                       
                    </select>
                  </div>
 
                  <div class="form-group">
                    <input type="submit"  name="send_quote" class="btn btn-default " value="Submit Quotation" >
                  </div>

                  </form>