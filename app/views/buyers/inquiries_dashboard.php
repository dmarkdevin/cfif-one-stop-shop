
<div class="row">
  <div class="col-xs-12">


    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Inquiries</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="table-responsive col-sm-12">
        <table id="example" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th></th>
            <th>Inquiry #</th>
            <th>Submitted Quotations</th>
            <th>Status</th>
             
          </tr>
          </thead>
          <tbody>
          <?php foreach ($data['list'] as $key => $value) { 

                $total_number_of_quotation  = "SELECT * FROM tbl_quotation WHERE inquiries_id='".$value['id']."' ";
                $total_number_of_quotation  = $this->db->getCount($total_number_of_quotation);
                
                $submitted_quotation      = "SELECT * FROM tbl_quotation WHERE inquiries_id='".$value['id']."' AND quote IS NOT NULL";
                $submitted_quotation      = $this->db->getCount($submitted_quotation);


                $days = days($value['date_added']);

                if($days > 3){
                  $status = inquiry_status('closed');
                }else{
                  $status =  inquiry_status($value['status']);
                }
 


            ?>
          <tr>
            <td><a class="btn btn-default btn-flat btn-xs" title="" tooltip href="<?=URL_ROOT;?>buyers/inquiry/<?=e($value['id']);?>"><i class="fa fa-info"></i> Preview</a></td>
            <td><?=($value['id']);?> <!-- <?=($value['date_added']);?>  --></td> 
            <td><?=$submitted_quotation.' / '.$total_number_of_quotation;?></td>
            <td><?=$status;?></td>
            
          </tr>
          <?php } ?> 
          </tbody>
         
        </table>
        </div>
      </div>
      <!-- /.box-body -->
            <div class="box-footer clearfix">
              <p  class="pull-right"><a href="<?=URL_ROOT;?>buyers/inquiries">View All Transactions</a> </p>
            </div>
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
      

 