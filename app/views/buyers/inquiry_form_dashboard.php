 <div class="row">
  <div class="col-xs-12">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Inquiry</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?=URL_ROOT;?>buyers/inquire" method="post" enctype="multipart/form-data">
              <div class="box-body">



              <div class="form-group">
              <label >Dimension (cm) *</label>
                <div class="row">
   
                  <div class="col-sm-4">
                    <input type="number" class="form-control" placeholder="Height" name="dimension_height" required>
                  </div>
                   <div class="col-sm-4">
                    <input type="number" class="form-control" placeholder="Width" name="dimension_width" required>
                  </div>
                  <div class="col-sm-4">
                    <input type="number" class="form-control" placeholder="Length" name="dimension_length" required>
                  </div>
                </div>
              </div>

                <div class="form-group">
                  <label >Quantity *</label>
                  <input type="number" class="form-control" name="quantity" min="1" required>
                </div>
                <div class="form-group">
                  <label >Target Price Per Unit (US Dollar) *</label>
                  <input type="number" class="form-control" name="target_price" min="1" required>
                </div>


                <div class="form-group">
                  <label >Materials *</label>
                  <select class="form-control" name="material" style="width: 100%;" required>
                      <option value=""></option>
                      <?php foreach ($data['materials'] as $key => $value) {

                      $selected = '';
                        
                        echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['name'].'</option>';
                      } ?> 

                   </select>
                </div>


                <div class="form-group">
                  <label >Lead Time *</label>
                    <select class="form-control select2 select2-hidden-accessible" name="lead_time" style="width: 100%;"  required>
                      

                        <option   value=""></option>
                        <option   value="1">Less Than 30 Days</option>
                        <option   value="2">30 - 60 Days</option>
                        <option   value="3">60 - 90 Days</option>
                        <option   value="4">over 90 Days</option>
                       
                    </select>
                </div>


                <div class="form-group">
                  <label >Type *</label>
                  <select class="form-control" name="company_type" style="width: 100%;" required>
                              

                      <option   value="" ></option>
                      <option   value="residential">Residential</option>
                      <option   value="commercial">Commercial</option>
                      <!-- <option   value="project">Project</option> -->
                      <option   value="any" selected>Any</option>
                     
                  </select>
                </div>
 


                 <div class="form-group">
                  <label>Description *</label>
                  <textarea class="form-control" rows="2" name="additional_info"></textarea>
                </div>

                <div class="form-group">
                  <label>Images / Drawing *</label>
                  <input type="file" name="image[]" multiple required>

                  <p class="help-block">Image file only.</p>
                </div>
 
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="submit" class="btn btn-primary">SEND INQUIRY <i class="fa fa-arrow-circle-right"></i></button>
              </div>
            </form>
          </div>
          <!-- /.box-->



            </div>
  <!-- /.col -->
</div>
<!-- /.row -->