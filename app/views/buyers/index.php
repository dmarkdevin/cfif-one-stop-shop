


<div class="row">
	<div class="col-md-12">
		<?php
			
			if(isset($data['message'])): 
				echo '	<div class="callout callout-info alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						 
						<p>Inquiry Successfully Sent</p>
						</div>';
			endif; 
		?>

	</div>
	
</div>

<div class="row">
  <div class="col-md-8">
    <?=$this->view('buyers/inquiries_dashboard',$data);?>
  </div>
  <div class="col-md-4">
    <?=$this->view('buyers/inquiry_form_dashboard',$data);?>
  </div>
</div>