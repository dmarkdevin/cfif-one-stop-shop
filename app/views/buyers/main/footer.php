      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.8
      </div>
      <strong>Copyright &copy; 2017 <a href="https://www.cebufurnitureindustries.com/" target="_blank">Cebu Furniture Industries</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?=ASSETS.BACK;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?=ASSETS.BACK;?>bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?=ASSETS.BACK;?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=ASSETS.BACK;?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=ASSETS.BACK;?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=ASSETS.BACK;?>dist/js/demo.js"></script>
</body>
</html>
