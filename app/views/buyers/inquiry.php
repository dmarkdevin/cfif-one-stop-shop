

      <div class="row">
        <div class="col-md-4"><?=$this->view('buyers/inquiry/inquiry_left',$data);?></div>
        <!-- /.col -->
        <div class="col-md-8">
          
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Description</h3>

                <span class="mailbox-read-time pull-right"><?=$data['inquiry']['date_added'];?></span>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
 
 
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                
                <p><?=$data['inquiry']['additional_info'];?></p>

              
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
 
          </div>
          <!-- /. box -->
          
          <?php

         

              if($data['inquiry']['status']=='closed'){
                $this->view('buyers/inquiry/selected_quotation',$data);
              }else{
                $this->view('buyers/inquiry/quotations',$data);
              }
           
                 
            
          ?>        


        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    