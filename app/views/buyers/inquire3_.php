
      <div class="row">
 
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Read Mail</h3>
              <span class="mailbox-read-time pull-right">15 Feb. 2016 11:03 PM</span>

<!--               <div class="box-tools pull-right">
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
              </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
            
                <div class="row">
                  <div class="col-sm-6  ">
                  
              
                    <div class="table-responsive">
                      <table class="table">
                        <tr>
                        <th style="width:50%">Material:</th>
                        <td><?=$data['inquiry']['material'];?></td>
                        </tr>
                        <tr>
                        <th>Quantity</th>
                        <td><?=$data['inquiry']['volume'];?></td>
                        </tr>
                        <tr>
                        <th>Dimension (HxWxL)</th>
                        <td><?=$data['inquiry']['dimension_height'];?> x <?=$data['inquiry']['dimension_width'];?> x <?=$data['inquiry']['dimension_length'];?> cm</td>
                        </tr>                        
                      </table>
                    </div>
                  </div> 
                  <div class="col-sm-6  ">
                  
              
                    <div class="table-responsive">
                      <table class="table">
                       
                        <tr>
                        <th>Lead Time:</th>
                        <td><?=$data['inquiry']['lead_time'];?></td>
                        </tr>
                        <tr>
                        <th>Price Per Unit:</th>
                        <td><?=$data['inquiry']['target_price'];?></td>
                        </tr>
                      </table>
                    </div>
                  </div> 

                </div>
              </div>
 
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                <!-- <p>Hello John,</p> -->

                <?=$data['inquiry']['additional_info'];?>
 
                <!-- <p>Thanks,<br>Jane</p> -->
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                  <div class="row">
                            <?php foreach ($data['images'] as $key => $image) {
                              # code...
                            ?>

                            <div class="col-sm-4">
                             <?=upload_images($image);?>
                            </div> 

                            <?php }  ?>                       
                     </div> 








 
            </div>
            <!-- /.box-footer -->
            <div class="box-footer">
              <div class="pull-right">
                <a type="button" class="btn btn-primary" href="<?=URL_ROOT;?>buyers/inquire4"> Confirm and Proceed</a>
                
              </div>
            
              <a type="button" class="btn btn-primary" href="<?=URL_ROOT;?>buyers/inquire2"><i class="fa fa-reply"></i> Back</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
        <div class="col-md-2"></div>
      </div>
      <!-- /.row -->
    