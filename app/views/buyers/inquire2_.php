 

<!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?=URL_ROOT;?>buyers/inquire" method="post" enctype="multipart/form-data" >
              <div class="box-body">
               <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                  <label>Images / Drawing *</label>
                  <input type="file" name="image[]" multiple >

                  <p class="help-block">Image file only, recommended size: 128x128px.
                    Upload new image only if you want to change..</p>
                </div>
                </div>


                <div class="col-md-6">
                  <div class="row">
                            <?php foreach ($data['images'] as $key => $image) {
                              # code...
                            ?>

                            <div class="col-sm-4">
                             <?=upload_images($image);?>
                            </div> 

                            <?php }  ?>                       
                     </div>                                         
                </div>
                </div>
                <div class="form-group">
                <label >Dimension (cm) *</label>
                  <div class="row">
     
                    <div class="col-xs-4">
                      <p>Height</p>
                      <input type="number" class="form-control" placeholder="Height" name="dimension_height" value="<?=$data['inquiry']['dimension_height'];?>" min="1" required>
                    </div>
                     <div class="col-xs-4">
                     <p>Width</p>
                      <input type="number" class="form-control" placeholder="Width" name="dimension_width" value="<?=$data['inquiry']['dimension_width'];?>" min="1" required>
                    </div>
                    <div class="col-xs-4">
                    <p>Length</p>
                      <input type="number" class="form-control" placeholder="Length" name="dimension_length" value="<?=$data['inquiry']['dimension_length'];?>" min="1" required>
                    </div>
                  </div>
                </div>


                <div class="form-group">
                  <label >Quantity *</label>
                  <input type="text" class="form-control" name="quantity" value="<?=$data['inquiry']['volume'];?>" min="1" required>
                </div>
                <div class="form-group">
                  <label >Target Price Per Unit (US Dollar) *</label>
                  <input type="number" class="form-control" name="target_price" value="<?=$data['inquiry']['target_price'];?>" min="1" required>
                </div>
                <div class="form-group">
                  <label >Materials *</label>
                  
                  <select class="form-control" name="material" style="width: 100%;" required>
                      <option value=""></option>
                      <?php foreach ($data['materials'] as $key => $value) {

                         $selected = ($data['inquiry']['material']==$value['id']) ? 'selected':'';
                        
                        echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['name'].'</option>';
                      } ?> 
 
                   </select>

                </div>
                <div class="form-group">
                  <label >Lead Time *</label>
                  <select class="form-control select2 select2-hidden-accessible" name="lead_time" style="width: 100%;"  required>
                  

                    <option  <?=($data['inquiry']['lead_time']=='') ? 'selected':'';?>  value=""></option>
                    <option  <?=($data['inquiry']['lead_time']=='1') ? 'selected':'';?> value="1">Less Than 30 Days</option>
                    <option  <?=($data['inquiry']['lead_time']=='2') ? 'selected':'';?> value="2">30 - 60 Days</option>
                    <option  <?=($data['inquiry']['lead_time']=='3') ? 'selected':'';?> value="3">60 - 90 Days</option>
                    <option  <?=($data['inquiry']['lead_time']=='4') ? 'selected':'';?> value="4">over 90 Days</option>
                   
                </select>
                </div>

                
                <div class="form-group">
                  <label >Type *</label>
                    <select class="form-control" name="company_type" style="width: 100%;" required>
                      

                        <option <?=($data['inquiry']['company_type']=='') ? 'selected':'';?> value="" ></option>
                        <option <?=($data['inquiry']['company_type']=='residential') ? 'selected':'';?>  value="residential">Residential</option>
                        <option <?=($data['inquiry']['company_type']=='commercial') ? 'selected':'';?>  value="commercial">Commercial</option>
                        <!-- <option <?=($data['inquiry']['company_type']=='project') ? 'selected':'';?>  value="project">Project</option> -->
                        <option <?=($data['inquiry']['company_type']=='any') ? 'selected':'';?>  value="any"  >Any</option>
                       
                    </select>
                </div>
 


                 <div class="form-group">
                  <label>Description *</label>
                  <textarea class="form-control" rows="2" name="additional_info"  ><?=$data['inquiry']['additional_info'];?></textarea>
                </div>

                
                


                
 
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="submit" class="btn btn-primary">UPDATE INQUIRY</button>
                <?php if($data['members']>0 && $data['count'] >0):?>
                  <a href="<?=URL_ROOT;?>buyers/inquire3" class="btn btn-primary pull-right">NEXT</a>
                <?php endif; ?>
              </div>
            </form>
          </div>
          <!-- /.box -->
 



 