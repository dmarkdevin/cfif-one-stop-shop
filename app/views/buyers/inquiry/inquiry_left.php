

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              

             
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Inquiry No.</b> <a class="pull-right"><?=$data['inquiry']['id'];?></a>
                </li>
                <li class="list-group-item">
                  <b>Status</b> <a class="pull-right">
                  
                  <?php 

                  if(isset($data['quotation']['quote'])){
                    if(isset($data['quotation']['status'])){
                      selected();
                    }else{
                      submitted();
                    }
                    
                  }else{
                     
                    $days = days($data['inquiry']['date_added']);

                      if($days > 3 && $data['inquiry']['status'] == ''){
                         echo inquiry_status('closed');
                      }else{
                        echo inquiry_status($data['inquiry']['status']);
                      } 

                    



                  } 

                  ?>

                  
                    
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Material</b> <a class="pull-right"><?=$data['inquiry']['material'];?></a>
                </li>
                <li class="list-group-item">
                  <b>Quantity</b> <a class="pull-right"><?=$data['inquiry']['volume'];?></a>
                </li>
                <li class="list-group-item">
                  <b>Lead Time</b> <a class="pull-right"><?=lead_time($data['inquiry']['lead_time']);?></a>
                </li>
                <li class="list-group-item">
                  <b>Target Price</b> <a class="pull-right"><?=$data['inquiry']['target_price'];?> USD</a>
                </li>
                <li class="list-group-item">
                  <b>Dimension (cm) (HxWxL)</b> <a class="pull-right"><?=$data['inquiry']['dimension_height'];?> x <?=$data['inquiry']['dimension_width'];?> x <?=$data['inquiry']['dimension_length'];?></a>
                </li>
         

                <?php if(isset($data['quotation']['quote'])){
                        if(isset($data['quotation']['status'])){

                           $buyer    = "SELECT * FROM tbl_buyers WHERE id = '".$data['inquiry']['user_id']."'";
                            $data['buyer']  = $this->db->getFetch($buyer);
                ?>

                <li class="list-group-item">
                  <button type="button" class="btn btn-sm btn-default btn-block" data-toggle="modal" data-target="#myModalx">Contact Information</button>
                                    <!-- Modal -->
                  <div id="myModalx" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title"><?=$data['buyer']['name'];?></h4>
                        </div>
                        <div class="modal-body">
                          <p><?=$data['buyer']['email'];?></p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>

                    </div>
                  </div>
                </li>

                 <?php
                        }
                      }

                ?>


              </ul>

               
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Images / Drawing</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
                    <div class="row">
                            <?php foreach ($data['images'] as $key => $image) {
                              # code...
                            ?>

                            <div class="col-sm-6">
                             <?=upload_images($image);?>
                            </div> 

                            <?php }  ?>                       
                     </div>





            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        